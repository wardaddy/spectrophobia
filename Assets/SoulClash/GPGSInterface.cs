﻿using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using UnityEngine;
using GooglePlayGames.BasicApi.SavedGame;
using System;
//using Google;
//using System.Threading.Tasks;

public class GPGSInterface : Singleton<GPGSInterface>
{
#if UNITY_ANDROID
    private static string GLOBAL_LEADERBOARD_ID = "CgkIzI3HzbcZEAIQAg";
    private static string GLOBAL_AUTOAIM_LEADERBOARD_ID = "CgkIzI3HzbcZEAIQAw";

    void Start()
    {
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
        // enables saving game progress.
        .EnableSavedGames()                                                   // TODO: Fix crash
        // requests the email address of the player be available.
        // Will bring up a prompt for consent.
        .RequestEmail()
        // requests a server auth code be generated so it can be passed to an
        //  associated back end server application and exchanged for an OAuth token.
        //.RequestServerAuthCode(false)
        // requests an ID token be generated.  This OAuth token can be used to
        //  identify the player to other services such as Firebase.
        //.RequestIdToken()
        .Build();

        PlayGamesPlatform.InitializeInstance(config);
        // recommended for debugging:
        PlayGamesPlatform.DebugLogEnabled = true;
        // Activate the Google Play Games platform
        PlayGamesPlatform.Activate();
    }

    public void loginGPGS()
    {
        // authenticate user:
        Social.localUser.Authenticate((bool success) => {
            // handle success or failure
            if (success)
            {
                CustomDebug.Log("Login Success");
                loadProgress();
            }
            else
                CustomDebug.Log("Login Failed");
        });
    }

    public bool isAuthenticated()
    {
        return Social.localUser.authenticated;
    }

    public void postScore(int score)
    {
        if (!isAuthenticated())
            return;
        // post score 12345 to leaderboard ID "Cfji293fjsie_QA")
        Social.ReportScore(score, GLOBAL_LEADERBOARD_ID, (bool success) => {
            // handle success or failure
            if (success)
                CustomDebug.Log("Score Submitted succesfully");
            else
                CustomDebug.Log("Score submission failed");
        });
    }

    public void postAutoAimScore(int score)
    {
        if (!isAuthenticated())
            return;
        Social.ReportScore(score, GLOBAL_AUTOAIM_LEADERBOARD_ID, (bool success) => {
            // handle success or failure
            if (success)
                CustomDebug.Log("Auto Aim Score Submitted succesfully");
            else
                CustomDebug.Log("Auto Aim Score submission failed");
        });
    }

    public void showGlobalLeaderboard()
    {
        if (!isAuthenticated())
        {
            loginGPGS();
            return;
        }
        PlayGamesPlatform.Instance.ShowLeaderboardUI();
    }

    bool m_save = false;

    public void uploadProgress()
    {
        if (!isAuthenticated())
        {
            loginGPGS();
            return;
        }

        m_save = true;
        openSavedGames("temp.dat");
    }

    public void loadProgress()
    {
        if (!isAuthenticated())
        {
            loginGPGS();
            return;
        }
        m_save = false;
        openSavedGames("temp.dat");

		GameObject loadingPopup = StateManager.getInstance().getPopupObject(StateManager.PopupType.POPUP_LOADING);
		loadingPopup.GetComponent<LoadingPopup>().setup(TextDefs.getText(TextDefs.TextId.TEXT_CHECKING_PROGRESS));
        StateManager.getInstance().pushPopup(StateManager.PopupType.POPUP_LOADING);
    }

    private void openSavedGames(string filename)
    {
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
        savedGameClient.OpenWithAutomaticConflictResolution(filename, DataSource.ReadCacheOrNetwork,
            ConflictResolutionStrategy.UseLongestPlaytime, OnSavedGameOpened);
    }

    private void OnSavedGameOpened(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            // handle reading or writing of saved game.
            CustomDebug.Log("SavedGame Request Status Success");
            if (m_save)
            {
                SaveGame(game, GameStats.getByteArray(), new TimeSpan(0));
            }
            else
            {
                // load game
                LoadGameData(game);
            }
        }
        else
        {
            // handle error
            if (StateManager.getInstance().getCurrentPopup() == StateManager.PopupType.POPUP_LOADING)
                StateManager.getInstance().popPopup();
            CustomDebug.Log("SavedGame Request Status Failed");
        }
    }

    private void SaveGame(ISavedGameMetadata game, byte[] savedData, TimeSpan totalPlaytime)
    {
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;

        SavedGameMetadataUpdate.Builder builder = new SavedGameMetadataUpdate.Builder();
        builder = builder
            .WithUpdatedPlayedTime(totalPlaytime)
            .WithUpdatedDescription("Saved game at " + DateTime.Now);
        
        SavedGameMetadataUpdate updatedMetadata = builder.Build();
        savedGameClient.CommitUpdate(game, updatedMetadata, savedData, OnSavedGameWritten);
    }

    private void OnSavedGameWritten(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            // handle reading or writing of saved game.
            CustomDebug.Log("Uploaded Game Progress Successfully");
            //loadProgress();
        }
        else
        {
            // handle error
            CustomDebug.Log("Game Progress Upload Failed");
            //DeleteGameData("temp.dat");
        }
    }

    private void LoadGameData(ISavedGameMetadata game)
    {
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
        savedGameClient.ReadBinaryData(game, OnSavedGameDataRead);
    }

    private void OnSavedGameDataRead(SavedGameRequestStatus status, byte[] data)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            // handle processing the byte array data
            if (GameStats.setGameStats(data) == false)
            {
                CustomDebug.Log("Server does not have latest progress. Upload progress to server");
                uploadProgress();
            }
            else
               CustomDebug.Log("Loaded Online Progress Successfully");
            //DeleteGameData("temp.dat");
        }
        else
        {
            // handle error
            CustomDebug.Log("Online Progress Loading Failed");
            //DeleteGameData("temp.dat");
        }

        if (StateManager.getInstance().getCurrentPopup() == StateManager.PopupType.POPUP_LOADING)
            StateManager.getInstance().popPopup();
    }

    private void DeleteGameData(string filename)
    {
        // Open the file to get the metadata.
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
        savedGameClient.OpenWithAutomaticConflictResolution(filename, DataSource.ReadCacheOrNetwork,
            ConflictResolutionStrategy.UseLongestPlaytime, DeleteSavedGame);
    }

    private static void DeleteSavedGame(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
            savedGameClient.Delete(game);
            CustomDebug.Log("Deleted Saved Game Successfully");
        }
        else
        {
            // handle error
            CustomDebug.Log("Saved Game Deletion Failed");
        }
    }
#endif
}
