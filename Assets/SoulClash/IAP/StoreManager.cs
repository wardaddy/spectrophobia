﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using System;

public class StoreManager : MonoBehaviour, IStoreListener {

	// Placing the Purchaser class in the CompleteProject namespace allows it to interact with ScoreManager, 
	// one of the existing Survival Shooter scripts.

	// Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.

	private static IStoreController m_StoreController;          // The Unity Purchasing system.
	private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

	public static StoreManager Instance;
	// Product identifiers for all products capable of being purchased: 
	// "convenience" general identifiers for use with Purchasing, and their store-specific identifier 
	// counterparts for use with and outside of Unity Purchasing. Define store-specific identifiers 
	// also on each platform's publisher dashboard (iTunes Connect, Google Play Developer Console, etc.)

	// General product identifiers for the consumable, non-consumable, and subscription products.
	// Use these handles in the code to reference which product to purchase. Also use these values 
	// when defining the Product Identifiers on the store. Except, for illustration purposes, the 
	// kProductIDSubscription - it has custom Apple and Google identifiers. We declare their store-
	// specific mapping to Unity Purchasing's AddProduct, below.

	void Awake()
	{
		Instance = this;
	}
	void Start()
	{
		// If we haven't set up the Unity Purchasing reference
		if (m_StoreController == null)
		{
			// Begin to configure our connection to Purchasing
			InitializePurchasing();
		}
	}

	public void InitializePurchasing()
	{
		// If we have already connected to Purchasing ...
		if (IsInitialized())
		{
			// ... we are done here.
			return;
		}

		// Create a builder, first passing in a suite of Unity provided stores.
		var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

		// Add a product to sell / restore by way of its identifier, associating the general identifier
		// with its store-specific identifiers.
		foreach (StoreDataManager.StoreData productData in StoreDataManager.Instance.m_productData)
			builder.AddProduct(productData.m_productId, ProductType.Consumable);

		// Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
		// and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
		UnityPurchasing.Initialize(this, builder);
	}


	public bool IsInitialized()
	{
		// Only say we are initialized if both the Purchasing references are set.
		return m_StoreController != null && m_StoreExtensionProvider != null;
	}

	public bool isAvailableForPurchase(string productId)
	{
		Product product = m_StoreController.products.WithID(productId);
		return (product != null) && (product.availableToPurchase);
	}	

		public void BuyConsumable(string productId)
		{
			// push purchase popup
			GameObject purchasePop = StateManager.getInstance().getPopupObject(StateManager.PopupType.POPUP_PROCESSING_PURCHASE);
			purchasePop.GetComponent<LoadingPopup>().setup(TextDefs.getText(TextDefs.TextId.TEXT_PROCESSING_PURCHASE));
			StateManager.getInstance().pushPopup(StateManager.PopupType.POPUP_LOADING);

			// Buy the consumable product using its general identifier. Expect a response either 
			// through ProcessPurchase or OnPurchaseFailed asynchronously.
			BuyProductID(productId);
    }

		void BuyProductID(string productId)
		{
			// If Purchasing has been initialized ...
			if (IsInitialized())
			{
				// ... look up the Product reference with the general product identifier and the Purchasing 
				// system's products collection.
				Product product = m_StoreController.products.WithID(productId);

				// If the look up found a product for this device's store and that product is ready to be sold ... 
				if (product != null && product.availableToPurchase)
				{
					CustomDebug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
					// ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
					// asynchronously.
					m_StoreController.InitiatePurchase(product);
                    UserTracker.Instance.LogEvent("IAP", "Buy", productId + " : "+ product.metadata.localizedPrice, 0);
            }
				// Otherwise ...
				else
				{
					// ... report the product look-up failure situation  
					CustomDebug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
					
					GameObject purchasePop = StateManager.getInstance().getPopupObject(StateManager.PopupType.POPUP_PURCHASE_FAILED);
					purchasePop.GetComponent<OKPopup>().setup(TextDefs.getText(TextDefs.TextId.TEXT_PURCHASE_FAILED));
					StateManager.getInstance().pushPopup(StateManager.PopupType.POPUP_PURCHASE_FAILED);
                    UserTracker.Instance.LogEvent("IAP", "Buy", "Failed : Either not purchasing product or not available", 0);
                 }
			}
			// Otherwise ...
			else
			{
				// ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
				// retrying initiailization.
				CustomDebug.Log("BuyProductID FAIL. Not initialized.");
				
				GameObject purchasePop = StateManager.getInstance().getPopupObject(StateManager.PopupType.POPUP_PURCHASE_FAILED);
				purchasePop.GetComponent<OKPopup>().setup(TextDefs.getText(TextDefs.TextId.TEXT_PURCHASE_FAILED));
				StateManager.getInstance().pushPopup(StateManager.PopupType.POPUP_PURCHASE_FAILED);
                UserTracker.Instance.LogEvent("IAP", "Buy", "Attempted but not initialized", 0);
        }
		}

		//  
		// --- IStoreListener
		//

		public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
		{
			// Purchasing has succeeded initializing. Collect our Purchasing references.
			CustomDebug.Log("OnInitialized: PASS");

			// Overall Purchasing system, configured with products for this application.
			m_StoreController = controller;
			// Store specific subsystem, for accessing device-specific store features.
			m_StoreExtensionProvider = extensions;

			initialiseProducts();
		}

	private void initialiseProducts()
	{
		foreach (StoreDataManager.StoreData data in StoreDataManager.Instance.m_productData)
		{
			Product product = m_StoreController.products.WithID(data.m_productId);
			data.m_title = product.metadata.localizedTitle;
			data.m_description = product.metadata.localizedDescription;
			data.m_price = product.metadata.localizedPriceString;
		}
	}

		public void OnInitializeFailed(InitializationFailureReason error)
		{
			// Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
			CustomDebug.Log("OnInitializeFailed InitializationFailureReason:" + error);
            UserTracker.Instance.LogEvent("IAP", "Store", "Initialization failed", 0);
        }


		public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
		{
		// A consumable product has been purchased by this user.
			string productId = args.purchasedProduct.definition.id;

			CustomDebug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

			if (StoreDataManager.Instance.onPurchased(productId))
			{
                GameStats.IAP_COUNTER += 1;

                StoreDataManager.StoreData storeData = StoreDataManager.Instance.getStoreData(productId);
				GameObject purchasePop = StateManager.getInstance().getPopupObject(StateManager.PopupType.POPUP_PURCHASE_SUCCESSFUL);
				string text = TextDefs.getText(TextDefs.TextId.TEXT_PURCHASE_SUCCESSFUL);
				text += "\n" + storeData.m_rewardData.m_amount + " ";
				text += TextDefs.getText(TextDefs.TextId.TEXT_SOULS);

				purchasePop.GetComponent<OKPopup>().setup(text);
				StateManager.getInstance().pushPopup(StateManager.PopupType.POPUP_PURCHASE_SUCCESSFUL);
                UserTracker.Instance.LogTransaction(args.purchasedProduct.transactionID, productId, (double)args.purchasedProduct.metadata.localizedPrice, args.purchasedProduct.metadata.isoCurrencyCode);
			}
		
			// Return a flag indicating whether this product has completely been received, or if the application needs 
			// to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
			// saving purchased products to the cloud, and when that save is delayed. 
			return PurchaseProcessingResult.Complete;
		}


		public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
		{
			// A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
			// this reason with the user to guide their troubleshooting actions.
			CustomDebug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
			
			GameObject purchasePop = StateManager.getInstance().getPopupObject(StateManager.PopupType.POPUP_PURCHASE_FAILED);
			purchasePop.GetComponent<OKPopup>().setup(TextDefs.getText(TextDefs.TextId.TEXT_PURCHASE_FAILED));
			StateManager.getInstance().pushPopup(StateManager.PopupType.POPUP_PURCHASE_FAILED);

        UserTracker.Instance.LogEvent("IAP", product.definition.id, "Purchase Failed : " + failureReason.ToString(), 0);
	    }

}
