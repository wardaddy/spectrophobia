﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoreDataManager : MonoBehaviour {
	public enum RewardType {
		REWARD_SOULS,
	}
	
	[System.Serializable]
	public class RewardData {
		public RewardType m_type;
		public int m_amount;
	}

	[System.Serializable]
	public class StoreData {
		public string m_productId;
		public string m_title;
		public string m_description;
		public string m_price;
		public RewardData m_rewardData;
	}

	public static StoreDataManager Instance;

	public List<StoreData> m_productData;

	void Awake()
	{
		Instance = this;
	}

	public bool hasProductId(string id)
	{
		foreach (StoreData data in m_productData)
		{
			if (data.m_productId == id)
				return true;
		}

		return false;
	}
	
	public bool onPurchased(string productId)
	{
		StoreData data = getStoreData(productId);

		if (data != null)
		{
			switch (data.m_rewardData.m_type)
			{
				case RewardType.REWARD_SOULS:
					GameStats.SOULS += data.m_rewardData.m_amount;
                    GPGSInterface.Instance.uploadProgress();
                    return true;
			}
		}
        
		CustomDebug.Log("OnPurchase : Invalid Product Id : " + productId);
		return false;
	}

	public StoreData getStoreData(string productId)
	{
		foreach (StoreData data in m_productData)
		{
			if (data.m_productId == productId)
				return data;
		}

		return null;
	}
}
