﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;

public class GoogleAdsInterface {

    private RewardBasedVideoAd m_rewardBasedVideoAd;
    private InterstitialAd m_interstitialAd;
    private static String AD_UNIT_ID_CONTINUE_GAME = "ca-app-pub-6537726950016872/5493362540"; // Prod Id
    //private static String AD_UNIT_ID_CONTINUE_GAME = "ca-app-pub-6537726950016872/5507079418"; // Test id
    private static String AD_UNIT_ID_INTERSTITIAL = "ca-app-pub-6537726950016872/8443006584";

    // Use this for initialization
    public void init () {
        m_rewardBasedVideoAd = RewardBasedVideoAd.Instance;

        // Called when an ad request has successfully loaded.
        m_rewardBasedVideoAd.OnAdLoaded += HandleRewardBasedVideoLoaded;
        // Called when an ad request failed to load.
        m_rewardBasedVideoAd.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
        // Called when an ad is shown.
        m_rewardBasedVideoAd.OnAdOpening += HandleRewardBasedVideoOpened;
        // Called when the ad starts to play.
        m_rewardBasedVideoAd.OnAdStarted += HandleRewardBasedVideoStarted;
        // Called when the user should be rewarded for watching a video.
        m_rewardBasedVideoAd.OnAdRewarded += HandleRewardBasedVideoRewarded;
        // Called when the ad is closed.
        m_rewardBasedVideoAd.OnAdClosed += HandleRewardBasedVideoClosed;
        // Called when the ad click caused the user to leave the application.
        m_rewardBasedVideoAd.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;

        m_interstitialAd = new InterstitialAd(AD_UNIT_ID_INTERSTITIAL);
        // Called when an ad request has successfully loaded.
        m_interstitialAd.OnAdLoaded += HandleOnAdLoaded;
        // Called when an ad request failed to load.
        m_interstitialAd.OnAdFailedToLoad += HandleOnAdFailedToLoad;
        // Called when an ad is shown.
        m_interstitialAd.OnAdOpening += HandleOnAdOpened;
        // Called when the ad is closed.
        m_interstitialAd.OnAdClosed += HandleOnAdClosed;
        // Called when the ad click caused the user to leave the application.
        m_interstitialAd.OnAdLeavingApplication += HandleOnAdLeftApplication;
    }

    public void requestRewardedVideo()
    {
#if UNITY_ANDROID
        string adUnitId = AD_UNIT_ID_CONTINUE_GAME;
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder()
            .AddTestDevice("C0A3B33E0004DADAF457AF37555CDD67")
            .Build();
        // Load the rewarded video ad with the request.
        RewardBasedVideoAd.Instance.LoadAd(request, adUnitId);

        if (CustomDebug.isDebugBuild())
        {
            List<string> testDevices = request.TestDevices;
            foreach (string str in testDevices)
                CustomDebug.Log("Test Devices : " + str);
        }
#endif

    }

    public void showRewardedAd()
    {
        //if (m_rewardBasedVideoAd.IsLoaded())
        {
            m_rewardBasedVideoAd.Show();
        }
    }

    public bool isLoaded()
    {
        return m_rewardBasedVideoAd.IsLoaded();
    }

    private void HandleRewardBasedVideoLeftApplication(object sender, EventArgs e)
    {
        CustomDebug.Log("Callback RewardBasedVideoLeftApplication : " + e.ToString());
    }

    private void HandleRewardBasedVideoClosed(object sender, EventArgs e)
    {
        CustomDebug.Log("Callback RewardBasedVideoClosed : " + e.ToString());
        AdsManager.OnRewardedAdClosed();
    }

    private void HandleRewardBasedVideoRewarded(object sender, Reward e)
    {
        CustomDebug.Log("Callback RewardBasedVideoRewarded : Type = "+ e.Type +"    Amount = " + e.Amount.ToString());   
        AdsManager.processRewardedAdRewarded(e.Type, e.Amount);
    }
    
    private void HandleRewardBasedVideoStarted(object sender, EventArgs e)
    {
        CustomDebug.Log("Callback RewardBasedVideoStarted : " + e.ToString());
        AdsManager.OnRewardedAdStarted();
    }

    private void HandleRewardBasedVideoOpened(object sender, EventArgs e)
    {
        CustomDebug.Log("Callback RewardBasedVideoOpened : " + e.ToString());
    }

    private void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs e)
    {
        CustomDebug.Log("Callback RewardBasedVideoFailedToLoad : " + e.Message);
        AdsManager.OnRewardedAdFailedToLoad();
    }
    
    private void HandleRewardBasedVideoLoaded(object sender, EventArgs e)
    {
        CustomDebug.Log("Callback RewardBasedVideoLoaded : " + e.ToString());
        AdsManager.OnRewardedAdLoaded();
    }

    /// <summary>
    /// Interstitial Ad Implementation
    /// </summary>
    public void loadInterstitialAd()
    {
        CustomDebug.Log("Load Interstitial");
        AdRequest request = new AdRequest.Builder()
            .AddTestDevice("C0A3B33E0004DADAF457AF37555CDD67")
            .Build();
        // Load the interstitial with the request.
        m_interstitialAd.LoadAd(request);
    }

    public void showInterstitialAd()
    {
        CustomDebug.Log("Show Interstitial : "+m_interstitialAd.IsLoaded());
        if (m_interstitialAd.IsLoaded())
            m_interstitialAd.Show();
    }

    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
        CustomDebug.Log("HandleAdLoaded event received");
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        CustomDebug.Log("HandleFailedToReceiveAd event received with message: "
                            + args.Message);
        AdsManager.onInterstitialLoadFailed();
    }

    public void HandleOnAdOpened(object sender, EventArgs args)
    {
        CustomDebug.Log("HandleAdOpened event received");
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        CustomDebug.Log("HandleAdClosed event received");
        AdsManager.onInterstitialClosed();
    }

    public void HandleOnAdLeftApplication(object sender, EventArgs args)
    {
        CustomDebug.Log("HandleAdLeftApplication event received");
    }
}
