﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    public enum SFX
    {
        SFX_FIRE,
        SFX_ITEM_COLLECTED,
        SFX_ENEMY_DEATH,
        SFX_PLAYER_DEATH,
        SFX_MISSILE,
        SFX_PLAYER_HURT,
        SFX_POWER_SPAWN,
        SFX_COUNT,
    }

    public enum MUSIC
    {
        MUSIC_BG_1,
        MUSIC_BG_MAIN_MENU,
        MUSIC_INVINCIBLE,
        MUSIC_COUNT,
    }

    private static AudioManager m_instance;
    public static AudioManager getInstance() { return m_instance; }

    public List<AudioSource> m_sfxSource;
    public List<AudioSource> m_musicSource;
    public List<AudioClip> m_sfxClip;
    public List<AudioClip> m_musicClip;

    private void Awake()
    {
        m_instance = this;
    }
    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < (int)MUSIC.MUSIC_COUNT; i++)
        {
            AudioSource audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.clip = m_musicClip[i];
            audioSource.playOnAwake = false;
            audioSource.loop = true;
            m_musicSource.Add(audioSource);
        }

        for (int i = 0; i < (int)SFX.SFX_COUNT; i++)
        {
            AudioSource audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.clip = m_sfxClip[i];
            audioSource.playOnAwake = false;
            audioSource.loop = false;
            audioSource.volume = 0.1f;
            m_sfxSource.Add(audioSource);
        }

        playMusic(MUSIC.MUSIC_BG_1);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void playSFX(SFX sfx) {
        if (GameSettings.SFX_ON == false)
            return;
        //if(m_sfxSource[(int)sfx].isPlaying == false)
        m_sfxSource[(int)sfx].PlayOneShot(m_sfxClip[(int)sfx]);

    }

    public void playMusic(MUSIC music, bool only = true)
    {
        if (GameSettings.MUSIC_ON == false)
            return;

        if (only)
        {
            stopAllMusic();
        }

        if (m_musicSource[(int)music].isPlaying == false)
        {
            m_musicSource[(int)music].Play();
        }
    }

    public void stopMusic(MUSIC music)
    {
        if (m_musicSource[(int)music].isPlaying)
            m_musicSource[(int)music].Stop();
    }

    public void stopSFX()
    {
        foreach (AudioSource source in m_sfxSource)
        {
            if (source.isPlaying)
                source.Stop();
        }
    }

    public void stopAllMusic()
    {
        foreach(var source in m_musicSource)
        {
            if (source.isPlaying)
                source.Stop();
        }
    }

}
