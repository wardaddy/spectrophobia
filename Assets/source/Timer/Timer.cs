﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;

public class Timer : MonoBehaviour {
    [System.Serializable]
    public class TimerData {
        public int m_startTime;
        public int m_duration;

        public TimerData() {
            m_startTime = 0;
            m_duration = 0;
        }

        public void start(int startTime, int duration)
        {
            m_startTime = startTime;
            m_duration = duration;
        }

        public void reset()
        {
            m_startTime = 0;
            m_duration = 0;
        }
        public float getRemainingTime() {
            float remainingTime = m_duration - (Timer.Current() - m_startTime);
            return remainingTime < 0 ? 0 : remainingTime;
        }
    }

    public enum TIMER_ID {
        TIMER_SOUL_REGENERATION,
        TIMER_COUNT,
    }

    private const string FILENAME = "/timerdata.dat";
    private static Timer m_instance;
    
    private List<TimerData> m_timersData = null;

    public static Timer getInstance() { return m_instance; }

    private void Awake()
    {
        m_instance = this;
    }
    // Use this for initialization
    void Start() {
     
    }

    // Update is called once per frame
    void Update() {

    }

    public bool isTimerRunning(TIMER_ID id) {
        return m_timersData[(int)id].m_startTime > 0;
    }

    public float getRemainingTime(TIMER_ID id) {
        return m_timersData[(int)id].getRemainingTime();
    }

    public void startTimer(TIMER_ID id, int startTime, int duration)
    {
        CustomDebug.Log("Start Timer for duration :" + duration);
        m_timersData[(int)id].start(startTime, duration);
    }

    public void resetTimer(TIMER_ID id) { m_timersData[(int)id].reset(); }

    public float getStartTime(TIMER_ID id) { return m_timersData[(int)id].m_startTime; }
    public float getDuration(TIMER_ID id) { return m_timersData[(int)id].m_duration; }

    /// <summary>
    /// Save Data Mechanism
    /// </summary>

    public static void saveData()
    {
        m_instance.printTimerData();
        BinaryFormatter bf = new BinaryFormatter();
        FileStream f = File.Create(Application.persistentDataPath + FILENAME);
        bf.Serialize(f, m_instance.m_timersData);
        f.Close();
    }

    void printTimerData()
    {
        CustomDebug.Log("Timer Data ");
        for (int i = 0; i < m_timersData.Count; i++)
        {
            TimerData t = m_timersData[i];
            CustomDebug.Log("ID : " + (TIMER_ID)i);
            CustomDebug.Log("Start Time : " + t.m_startTime);
            CustomDebug.Log("Duration : " + t.m_duration);
        }
    }

    public static void loadData()
    {
        if(m_instance.m_timersData == null)
        {
            m_instance.m_timersData = new List<TimerData>();
            for (int i = 0; i < (int)TIMER_ID.TIMER_COUNT; i++)
                m_instance.m_timersData.Add(new TimerData());
        }

        if (File.Exists(Application.persistentDataPath + FILENAME))
        {
            CustomDebug.Log("Loading Timer data");
            BinaryFormatter bf = new BinaryFormatter();
            FileStream f = File.Open(Application.persistentDataPath + FILENAME, FileMode.Open);
            List<TimerData> data = (List<TimerData>)bf.Deserialize(f);
            for(int i = 0; i < data.Count; i++)
            {
                m_instance.m_timersData[i] = data[i];
            }
            m_instance.printTimerData();
            m_instance.checkAllTimers();
            f.Close();
        }
        else
        {
            CustomDebug.Log("TimerData File does not exist");
        }
    }

    private void checkAllTimers()
    {
        for (int i = 0; i < (int)TIMER_ID.TIMER_COUNT; i++)
        {
            switch ((TIMER_ID)i)
            {
                case TIMER_ID.TIMER_SOUL_REGENERATION:
                    {
                        if (isTimerRunning(TIMER_ID.TIMER_SOUL_REGENERATION))
                        {
                            
                            int timeElapsed = Current() - m_timersData[i].m_startTime;
                            CustomDebug.Log("Time elapsed :" + timeElapsed);
                            int count = (timeElapsed >= PlayerDefs.CONST_SOUL_GENERATION_TIME) ? (timeElapsed / PlayerDefs.CONST_SOUL_GENERATION_TIME) : 0;
                            CustomDebug.Log("Count to be earned :" + count);
                            GameStats.SOULS += count;

                            if (GameStats.SOULS < PlayerDefs.CONST_MAX_SOUL)
                            {
                                m_timersData[i].reset();
                                timeElapsed -= (PlayerDefs.CONST_SOUL_GENERATION_TIME * count);
                                //CustomDebug.Log("Start Timer for duration : " + (PlayerDefs.CONST_SOUL_GENERATION_TIME - timeElapsed);
                                startTimer(TIMER_ID.TIMER_SOUL_REGENERATION, Current() - timeElapsed, PlayerDefs.CONST_SOUL_GENERATION_TIME);
                            }
                        }
                    }
                    break;

            }
        }
    }

    public static int Current()
    {
        DateTime epochStart = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        int currentEpochTime = (int)(DateTime.UtcNow - epochStart).TotalSeconds;

        return currentEpochTime;
    }
}
