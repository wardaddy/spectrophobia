﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class GameSettings : MonoBehaviour {

    private const string FILENAME = "/gamesettings.dat";

    [System.Serializable]
    struct Data {
        public bool m_autoAim;
        public bool m_SFXOn;
        public bool m_MusicOn;
    }

    private static Data m_data;

    private void Start()
    {
        
    }

    private void OnDestroy()
    {
     
    }

    public static void saveData()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream f = File.Create(Application.persistentDataPath + FILENAME);
        bf.Serialize(f, m_data);
        f.Close();
    }

    public static void loadData()
    {
        CustomDebug.Log(Application.persistentDataPath + FILENAME);
        if (File.Exists(Application.persistentDataPath + FILENAME))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream f = File.Open(Application.persistentDataPath + FILENAME, FileMode.Open);
            m_data = (Data)bf.Deserialize(f);
            f.Close();
        }
        else
        {
            m_data.m_SFXOn = true;
            m_data.m_MusicOn = true;
            m_data.m_autoAim = true;
            CustomDebug.Log("Game Settings File does not exist");
        }
    }

    public static bool AUTOAIM
    {
        get
        {
            return m_data.m_autoAim;
        }
        set
        {
            m_data.m_autoAim = value;
        }
    }

    public static bool SFX_ON
    {
        get
        {
            return m_data.m_SFXOn;
        }
        set
        {
            m_data.m_SFXOn = value;
        }
    }

    public static bool MUSIC_ON
    {
        get
        {
            return m_data.m_MusicOn;
        }
        set
        {
            m_data.m_MusicOn = value;
        }
    }
}
