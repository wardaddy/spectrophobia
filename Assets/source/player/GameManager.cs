﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using GooglePlayGames;
//using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

[System.Serializable]
public class ParticleDictionary : DictionaryTemplate<GameObject, int> { }

/// <summary>
/// /class : GameManager
/// /brief : starting point of game. handles add on functionalities such as pause game
/// </summary>
public class GameManager : MonoBehaviour {

    public enum GameplayMode {
        SINGLE_PLAYER,
        MULTIPLAYER,
    }

    private GameplayMode m_mode = GameplayMode.SINGLE_PLAYER;

	public GameObject m_startBtn; // start button

	private bool m_isPaused = true;
	private static GameManager m_instance;
	public Player	m_player;
    public GameObject m_mainCamera;

    // global score
    private int m_globalScore = 0;
    private int m_xp = 1;

    public List<ParticleDictionary> m_particlePrefabList;
    public int m_particlePreloadCount = 100;
    private NetworkObjectPool m_networkObjectPool;
    public GameObject m_rightJoystick;
    public GameObject m_leftJoystick;

    public NetworkObjectPool getNetworkPool() { return m_networkObjectPool; }

	public static GameManager getInstance() {
		return m_instance;
	}

	void Awake() {
		m_instance = this;
		foreach (var particlePrefab in m_particlePrefabList)
		{
			ObjectPool.Preload(particlePrefab._key, particlePrefab._value);
		}
	}
	// Use this for initialization
	void Start () {

        UserTracker.Instance.init();
        loadSaveData();

        UserTracker.Instance.sendEvent("Game", new Dictionary<string, object>
        {
            { "Action", "Launch"},
            { "Soul", GameStats.SOULS}
        });

        UserTracker.Instance.LogEvent("Launch", "Game Launch", "Soul", GameStats.SOULS);

        StateManager.getInstance().pushState(StateManager.MenuState.STATE_MAIN);

#if UNITY_ANDROID
            StartCoroutine(startLoginGPGS());
            AdsManager.init();
            AdsManager.requestRewardedAd();
            AdsManager.requestInterstitialAd();
        #endif
    }

    IEnumerator startLoginGPGS()
    {
        yield return new WaitForSeconds(1.0f);
        GPGSInterface.Instance.loginGPGS();
    }

    private void loadSaveData()
    {
        GameStats.loadStats();
        GameSettings.loadData();
        Timer.loadData();
        TutorialManager.loadData();
    }

    private void saveSaveData()
    {
        GameStats.saveStats();
        GameSettings.saveData();
        Timer.saveData();
        TutorialManager.saveData();
    }

    private void OnApplicationFocus(bool focus)
    {
        CustomDebug.Log("On Application Focus : " + focus);
        if (!focus)
            saveSaveData();

        UserTracker.Instance.sendEvent("Game", new Dictionary<string, object> {
            { "Action", "Focus Changed"},
            { "Focus", focus}
        });

        UserTracker.Instance.LogEvent("Game", "Interrupt", "On Application Focus", focus ? 1 : 0);
    }

    private void OnApplicationQuit()
    {
        CustomDebug.Log("On Application Quit");
        saveSaveData();
    }
    // Update is called once per frame
    void Update () {

        if (GameStats.SOULS < PlayerDefs.CONST_MAX_SOUL && !Timer.getInstance().isTimerRunning(Timer.TIMER_ID.TIMER_SOUL_REGENERATION))
            Timer.getInstance().startTimer(Timer.TIMER_ID.TIMER_SOUL_REGENERATION, Timer.Current(), PlayerDefs.CONST_SOUL_GENERATION_TIME);

        if (Timer.getInstance().isTimerRunning(Timer.TIMER_ID.TIMER_SOUL_REGENERATION) && Timer.getInstance().getRemainingTime(Timer.TIMER_ID.TIMER_SOUL_REGENERATION) == 0)
        {
            GameStats.SOULS += 1;
            Timer.getInstance().resetTimer(Timer.TIMER_ID.TIMER_SOUL_REGENERATION);
        }
    }
    
    // on quit/game over
	public void endGame()
	{
        pauseGame(true);
        StateManager.getInstance().pushState(StateManager.MenuState.STATE_RETRY);
	}

    public void exitGame()
    {
        pauseGame(true);
        reset();
        StateManager.getInstance().pushState(StateManager.MenuState.STATE_MAIN);
    }

	public bool isGamePaused()
	{
		return m_isPaused;
	}

    public void pauseGame(bool value)
    {
        m_isPaused = value;
        if (m_isPaused)
        {
            m_leftJoystick.GetComponent<Joystick>().setAlpha(1.0f);
            m_rightJoystick.GetComponent<Joystick>().setAlpha(1.0f);
        }
    }

    public void addScore(int score)
    {
        m_globalScore += score * m_xp;
    }

    public int getGlobalScore() { return m_globalScore; }

    public void incrementXP(int value = 1)
    {
        m_xp += value;
        m_player.onChangedXP();
    }

    public int getXp() { return m_xp; }

    public void setGameplayMode(GameplayMode mode)
    {
        m_mode = mode;
    }

    public GameplayMode getGameplayMode() { return m_mode; }

    public bool isMultiplayer()
    {
        return (m_mode == GameplayMode.MULTIPLAYER);
    }

    public bool isSinglePlayer()
    {
        return (m_mode == GameplayMode.SINGLE_PLAYER);
    }

    public bool isServer()
    {
        return m_player.isServer;
    }

    public void onTeamSelect(Player.Player_Team team)
    {
        CustomDebug.Log("On Team Select");
        ClanManager.getInstance().SelectedTeam = team;
        startGame();
    }

    public void onStartLocalPlayer(GameObject obj)
    {
        m_player = obj.GetComponent<Player>();
        m_networkObjectPool = m_player.GetComponent<NetworkObjectPool>();
        //EnemyManager.getInstance().preload();
        if (isSinglePlayer())
        {
            startGame();
        }
#if ENABLE_MULTIPLAYER
        else if (isMultiplayer())
            StateManager.getInstance().pushState(StateManager.MenuState.STATE_TEAM_SELECTION);
#endif
    }

    private void startGame()
    {
        CustomDebug.Log("Start Game 1");
        pauseGame(false);
        m_player.onStartGame();
        m_mainCamera.GetComponent<CameraFollower>().setTarget(m_player.transform);

        CustomDebug.Log("Auto Aim Enabled : " + GameSettings.AUTOAIM);
        enableRightJoystick(GameSettings.AUTOAIM == false);
        EnemyManager.getInstance().reset();
        EnemyManager.getInstance().setPlayer(m_player.transform);
        StateManager.getInstance().pushState(StateManager.MenuState.STATE_HUD);
        ItemManager.getInstance().usedCandy(PlayerDefs.CONST_START_GAME_PRICE);
        BulletManager.getInstance().setOnlineManager(m_player.gameObject);
        AudioManager.getInstance().playMusic(AudioManager.MUSIC.MUSIC_BG_1);

        AdsManager.m_interstitialAdCounter++;

        UserTracker.Instance.sendEvent("Game", new Dictionary<string, object> {
                {"Action", "Play"},
                { "Souls", GameStats.SOULS},
                { "Auto Aim", GameSettings.AUTOAIM }
            });

        
    }

    private void reset()
    {
        EnemyManager.getInstance().reset();
        m_globalScore = 0;
        m_player.resetPlayer();
        m_player = null;
        m_xp = 1;
        m_mainCamera.GetComponent<CameraFollower>().setTarget(null);
    }

    public void continueGame()
    {
        //Add invincible power
        ItemManager.getInstance().generateItem(m_player.transform.position, ItemManager.ITEM_TYPE.ITEM_INVINCIBLE);

        GameManager.getInstance().pauseGame(false);
        GameManager.getInstance().m_player.onResumeGame();
        ItemManager.getInstance().usedCandy(PlayerDefs.CONST_RETRY_GAME_PRICE);
        StateManager.getInstance().pushState(StateManager.MenuState.STATE_HUD);
    }

    public void startSinglePlayerGame()
    {
       setGameplayMode(GameManager.GameplayMode.SINGLE_PLAYER);
        DerivedNetworManager.getInstance().startHost();
    }

    public void enableRightJoystick(bool enable)
    {
        m_rightJoystick.GetComponent<Image>().enabled = enable;
    }
}
