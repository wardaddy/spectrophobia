﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.InteropServices;
using System;

/*
 0 - initial
 1 - added Save version
      */

public class GameStats : MonoBehaviour {

    private static string FILENAME = "/gamedata.dat";
    private static int DATA_VERSION = 1;
    private static bool m_isDirty = false;

    [System.Serializable]
    public struct Data {
        public int m_souls;
        public int m_highScore;
        public long m_saveVersion;
        public int m_dataVersion;
        public int m_IAPCounter;
    }

    public static Data m_data;

    private void Start()
    {
    
    }

    private void OnDestroy()
    {
      
    }

    private void Update()
    {
        if (m_isDirty)
        {
            saveStats();
#if UNITY_ANDROID
            if(GPGSInterface.Instance.isAuthenticated())
#endif
            syncProgress();

            m_isDirty = false;
        }
    }

    private void OnApplicationFocus(bool focus)
    {
        CustomDebug.Log("OnApplicationFocus : " + focus);
        if (focus)
        {
            m_isDirty = true;
            loadStats();
        }
        else
            saveStats();
        
    }

    private void OnApplicationPause(bool pause)
    {
        CustomDebug.Log("OnApplicationPause : " + pause);
    }

    private void OnApplicationQuit()
    {
        CustomDebug.Log("OnApplicationQuit");
    }

    public static void saveStats()
    {
        //CustomDebug.Log("Saving Game stats");
        m_data.m_dataVersion = DATA_VERSION;
        m_data.m_saveVersion++; //increment on each save

        try
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream f = File.Create(Application.persistentDataPath + FILENAME);
            bf.Serialize(f, m_data);
            f.Close();
        }
        catch (Exception e) { CustomDebug.Log("Exception on saving game stats: " + e.Message); }
    }

    public static void loadStats()
    {
        CustomDebug.Log("Loading Game stats");
        if (File.Exists(Application.persistentDataPath + FILENAME))
        {
            try
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream f = File.Open(Application.persistentDataPath + FILENAME, FileMode.Open);
                m_data = (Data)bf.Deserialize(f);
                f.Close();
            }
            catch (Exception e) { CustomDebug.Log("Exception while loading stats : " + e.Message); }
            
        }
        else
        {
            m_data.m_saveVersion = 0;
            m_data.m_souls = 10;
            m_data.m_IAPCounter = 0;
            m_isDirty = true;
            CustomDebug.Log("Game Stats File does not exist");
        }

    }

    private void syncProgress()
    {
        //#if UNITY_ANDROID
        //    GPGSInterface.Instance.loadProgress();
        //#endif
    }

    public static int SOULS
    {
        get
        {
            return m_data.m_souls;
        }
        set
        {
            m_isDirty = true;
            m_data.m_souls = value;
        }
    }

    public static int HIGHSCORE
    {
        get
        {
            return m_data.m_highScore;
        }
        set
        {
            m_isDirty = true;
            m_data.m_highScore = value;
        }
    }

    public static int IAP_COUNTER
    {
        get
        {
            return m_data.m_IAPCounter;
        }
        set
        {
            m_isDirty = true;
            m_data.m_IAPCounter = value;
        }
    }

    public static byte[] getByteArray()
    {
        int len = Marshal.SizeOf(m_data);

        byte[] arr = new byte[len];

        IntPtr ptr = Marshal.AllocHGlobal(len);

        Marshal.StructureToPtr(m_data, ptr, true);

        Marshal.Copy(ptr, arr, 0, len);

        Marshal.FreeHGlobal(ptr);
        CustomDebug.Log("Upload Data Length : " + len + "       Byte Array : " + arr.Length);
        CustomDebug.Log("Souls : " + SOULS + "      Highscore : " + HIGHSCORE);
        return arr;
    }

    public static bool setGameStats(byte[] byteArray)
    {
        bool updated = false;
        Data data = new Data();
        int len = Marshal.SizeOf(data);
        CustomDebug.Log("Data Length : " + len + "     Byte array Length : " + byteArray.Length);
        IntPtr i = Marshal.AllocHGlobal(len);

        Marshal.Copy(byteArray, 0, i, byteArray.Length);

        data = (Data)Marshal.PtrToStructure(i, m_data.GetType());
        Marshal.FreeHGlobal(i);

        updated = syncFromServer(data);
        printGameData();

        if (updated)
            m_data = data;

        
        return updated;
    }

    static bool syncFromServer(Data data)
    {
        CustomDebug.Log("sync From Server : Local versio : " + m_data.m_saveVersion + "     Server saveversion : " + data.m_saveVersion);
        return (m_data.m_saveVersion <= data.m_saveVersion);
    }

    static void printGameData()
    {
        CustomDebug.Log("GameStats: Souls : " + SOULS);
        CustomDebug.Log("GameStats: Highscore : " + HIGHSCORE);
        CustomDebug.Log("GameStats: SaveVersion : " + m_data.m_saveVersion);
        CustomDebug.Log("GameStats: DataVersion : " + m_data.m_dataVersion);
    }
}
