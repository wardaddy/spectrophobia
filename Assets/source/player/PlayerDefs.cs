﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDefs {

    public static int CONST_ITEM_SPAWN_RATE = 8;
    public static int CONST_SPECIAL_ITEM_SPAWN_COUNT = 4;
    public static int CONST_START_GAME_PRICE = 1;
    public static int CONST_RETRY_GAME_PRICE = 2;
    public static float CONST_MAX_FIRE_SPEED = 1.0f;
    public static int CONST_PLAYER_MAX_HEALTH = 15;

    // Enemy upgrade factor
    public static float CONST_ENEMY_HEALTH_UPGRADE_FACTOR = 0.1f;
    public static float CONST_ENEMY_SPEED_UPGRADE_FACTOR = 0.001f;
    public static float CONST_ENEMY_DAMAGE_UPGRADE_FACTOR = 0.1f;
    public static float CONST_ENEMY_COUNT_UPGRADE_FACTOR = 0.1f;

    //player upgrade factor
    public static float CONST_PLAYER_SPEED_UPGRADE_FACTOR = 0.001f;
    public static float CONST_PLAYER_DAMAGE_UPGRADE_FACTOR = 0.1f;
    public static float CONST_PLAYER_HEALTH_UPGRADE_FACTOR = 0.1f;

    // Max Factor
    public static float CONST_SPEED_MAX_FACTOR = 0.05f;
    public static float CONST_HEALTH_MAX_FACTOR = 5.0f;
    public static float CONST_DAMAGE_MAX_FACTOR = 5.0f;
    public static float CONST_COUNT_MAX_FACTOR = 10.0f;

    // Soul generation time
    public static int CONST_SOUL_GENERATION_TIME = 10 * 60; //minutes
    public static int CONST_MAX_SOUL = 5;
}
