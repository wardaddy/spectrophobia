﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutlineParticleEmitter : BaseParticleEmitter {

    Vector2[] m_points;
    // Use this for initialization
    int m_index;
    float m_deltaPos = 0.0f;
    public float m_speed = 0.1f;

    public bool alternate = true;
    int nextIndex = 1;
    protected override void Start()
    {
        base.Start();
        PolygonCollider2D col = GetComponent<PolygonCollider2D>();
        m_points = col.GetPath(0);

    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }

    protected override void spawnParticle()
    {
        base.spawnParticle();
        spawnAlongOutline();
    }

    void spawnAlongOutline()
    {
        Vector2 spawnPoint = Vector2.zero;

        if(m_spawnStyle == ParticleObject.SpawnStyle.RANDOM)
        {
            int pointIndex = Random.Range(0, m_points.Length);

            Vector2 pointA = m_points[pointIndex];
            Vector2 pointB = m_points[(pointIndex + 1) % m_points.Length];

            spawnPoint = Vector2.Lerp(pointA, pointB, Random.Range(0.0f, 1.0f));
        }
        else if(m_spawnStyle == ParticleObject.SpawnStyle.LINEAR)
        {
            int pointIndex = m_index;

            Vector2 pointA = m_points[pointIndex];
            Vector2 pointB = m_points[(pointIndex + nextIndex) % m_points.Length];

            spawnPoint = Vector2.Lerp(pointA, pointB, m_deltaPos);
            m_index += 1;
            if (m_index >= m_points.Length)
            {
                m_index = 0;
            }
            m_deltaPos += m_speed;
            if (m_deltaPos > 1.0f) m_deltaPos = 0.0f;
        }

        // Fixed issue : spawn point should change according to local scale.
        spawnPoint.x *= transform.localScale.x;
        spawnPoint.y *= transform.localScale.y;

        spawnAtPosition(spawnPoint + (Vector2)this.transform.position, Quaternion.identity);
    }

}
