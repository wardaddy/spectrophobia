﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextDefs {

    public enum LangId {
        LANG_EN,
        LANG_FR
    }

    public enum TextId {
        TEXT_NOT_ENOUGH_SOUL,
        TEXT_NO_ADS,
        TEXT_RECEIVED_REWARD,
        TEXT_WATCH_AD,
        TEXT_TUTORIAL_AUTO_AIM,
        TEXT_TUTORIAL_HEALTH,
        TEXT_TUTORIAL_JOYSTICK_LEFT,
        TEXT_TUTORIAL_JOYSTICK_RIGHT,
        TEXT_TUTORIAL_LIFE_REGENRATION,
        TEXT_TUTORIAL_PLAY,
        TEXT_TUTORIAL_POWER,
        TEXT_TUTORIAL_POWER_UP,
        TEXT_TUTORIAL_SETTINGS,
        TEXT_EXIT,
		TEXT_CHECKING_PROGRESS,
		TEXT_PROCESSING_PURCHASE,
		TEXT_PURCHASE_SUCCESSFUL,
		TEXT_PURCHASE_FAILED,
		TEXT_SOULS,
        TEXT_CONTINUE,
    }

    /*
     Dictionary Structure:
     {Text Id ,  {LANG_EN,
                  LANG_FR}}
         */

    private static Dictionary<TextId, List<string>> m_textData = new Dictionary<TextId, List<string>>()
        {
            { TextId.TEXT_NOT_ENOUGH_SOUL, new List<string>() { "Not enough souls in inventory.",
                                             "Pas assez d'âmes en inventaire."
                                           }
            },
            { TextId.TEXT_NO_ADS,          new List<string>() { "No Ad Available.",
                                             "Aucune annonce disponible."
                                           }
            },
            { TextId.TEXT_RECEIVED_REWARD, new List<string>() { "Received Reward : ",
                                             "Récompense reçue: "
                                           }
            },
            { TextId.TEXT_WATCH_AD,        new List<string>() { "Watch Ad To Continue",
                                             "Regarder l'annonce pour continuer"
                                           }
            },
            { TextId.TEXT_TUTORIAL_AUTO_AIM,new List<string>() { "Toggle between Auto aim and manual aim",
                                               "Toggle between Auto aim and manual aim"
                                             }
            },
            { TextId.TEXT_TUTORIAL_HEALTH, new List<string>() { "You will be exhausted with time. Keep on killing enemies to gain health.",
                                               "You will be exhausted with time. Keep on killing enemies to gain health."
                                             }
            },
            { TextId.TEXT_TUTORIAL_JOYSTICK_LEFT, new List<string>() { "Use Left Joystick to move around!!",
                                               "Use Left Joystick to move around!!"
                                             }
            },
            { TextId.TEXT_TUTORIAL_JOYSTICK_RIGHT,  new List<string>()  { "Use Right Joystick to aim and shoot",
                                               "Use Right Joystick to aim and shoot"
                                             }
            },
            { TextId.TEXT_TUTORIAL_LIFE_REGENRATION, new List<string>()   { "Patience!! You will get another chance.",
                                               "Patience!! You will get another chance."
                                             }
            },
            { TextId.TEXT_TUTORIAL_PLAY, new List<string>()   { "click Start to begin adventure!!",
                                               "click Start to begin adventure!!"
                                             }
            },
            { TextId.TEXT_TUTORIAL_POWER, new List<string>()   { "With great power comes great responsibility. But it lasts for few seconds",
                                               "With great power comes great responsibility. But it lasts for few seconds"
                                             }
            },
            { TextId.TEXT_TUTORIAL_POWER_UP, new List<string>()   { "collect power up to reveal their strength",
                                               "collect power up to reveal their strength"
                                             }
            },
            { TextId.TEXT_TUTORIAL_SETTINGS, new List<string>()   { "Checkout settings section",
                                               "Checkout settings section"
                                             }
            },
            { TextId.TEXT_EXIT, new List<string>()   { "Do you want to exit game?",
                                                       "Voulez-vous quitter le jeu?"
                                                     }
            },
			{ TextId.TEXT_CHECKING_PROGRESS, new List<string>()   { "Checking Progress",
													   "Checking Progress"
													 }
			},
			{ TextId.TEXT_PROCESSING_PURCHASE, new List<string>()   { "Processing Purchase",
													   "Processing Purchase"
													 }
			},
			{ TextId.TEXT_PURCHASE_SUCCESSFUL, new List<string>()   { "Purchased Successfully",
													   "Purchased Successfully"
													 }
			},
			{ TextId.TEXT_PURCHASE_FAILED, new List<string>()   { "Purchase Failed",
													   "Purchase Failed"
													 }
			},
			{ TextId.TEXT_SOULS, new List<string>()   { "Souls",
													   "Souls"
													 }
			},
            { TextId.TEXT_CONTINUE, new List<string>()   { "Continue : ",
                                                       "Continue : "
                                                     }
            },
        };

    public static string getText(TextId id)
    {
        return m_textData[id][(int)getLanguageId()];
    }

    private static LangId getLanguageId()
    {
        return LangId.LANG_EN;
    }

}
