﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToTarget : MonoBehaviour {

    private Transform m_target = null;
    public float m_speed = 1.0f;
	// Use this for initialization
	void Start () {
		
	}

    public void setup(Transform target)
    {
        m_target = target;
    }

	// Update is called once per frame
	void Update () {

        if (m_target == null)
            return;

        transform.position = Vector3.MoveTowards(transform.position, m_target.position, Time.deltaTime * m_speed);

        if (Vector3.Distance(transform.position, m_target.position) < 0.5f)
            Destroy(gameObject);
	}
}
