﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdsManager : MonoBehaviour{
    static GoogleAdsInterface m_googleAdsInterface = new GoogleAdsInterface();

    public enum AD_REQUEST_ID {
        AD_REQUEST_NONE,
        AD_REQUEST_CONTINUE_GAME,
        AD_REQUEST_MAINMENU,
    }

    static AD_REQUEST_ID m_lastRequestId = AD_REQUEST_ID.AD_REQUEST_NONE;
    private static bool m_adLoaded = false;
    private static bool m_hasRequested = false;
    private static bool m_processReward = false;
    private static string m_rewardString = "";
    private static double m_rewardValue = 0;

    public static int INTERSTIIAL_SHOW_AD_COUNTER = 3;
    public static int m_interstitialAdCounter = 0;

    public static void init()
    {
        m_googleAdsInterface.init();
    }

    private void Update()
    {
        if (m_processReward)
        {
            OnRewardedAdRewarded(m_rewardString, m_rewardValue);
            m_processReward = false;
        }
    }

    // Request
    public static void requestRewardedAd()
    {
        if (m_hasRequested)
        {
            CustomDebug.Log("Ad Already requested");
            return;
        }
        CustomDebug.Log("Request Rewarded Ad");
        m_googleAdsInterface.requestRewardedVideo();
        m_adLoaded = false;
        m_hasRequested = true;
        m_lastRequestId = AD_REQUEST_ID.AD_REQUEST_NONE;
    }

    public static bool showRewardedAd(AD_REQUEST_ID requestId)
    {
        CustomDebug.Log("show rewarde ad");
        if (m_adLoaded)
        {
            CustomDebug.Log("Ad lOaded");
            m_lastRequestId = requestId;
            m_googleAdsInterface.showRewardedAd();
            m_hasRequested = false;
            UserTracker.Instance.sendEvent("Ads", new Dictionary<string, object> {
                {"Request Id", requestId}
            });

            UserTracker.Instance.LogEvent("Ads", "Show Ad", "Rewarded Video", 1);
            return true;
        }
        return false;
    }

    static void processRequestCallback(string type, double amount)
    {
        switch (m_lastRequestId)
        {
            case AD_REQUEST_ID.AD_REQUEST_CONTINUE_GAME:
            case AD_REQUEST_ID.AD_REQUEST_MAINMENU:
                {
                    GameObject state = StateManager.getInstance().getPopupObject(StateManager.PopupType.POPUP_RECEIVED_REWARD);
                    string text = TextDefs.getText(TextDefs.TextId.TEXT_RECEIVED_REWARD) + (int)amount + " "+ type;
                    state.GetComponent<OKPopup>().setup(text);
                    StateManager.getInstance().pushPopup(StateManager.PopupType.POPUP_RECEIVED_REWARD);
                }
                break;
        }

        requestRewardedAd();
    }

    // Callback

    public static void OnRewardedAdClosed()
    {
        if(!m_processReward) // ad closed without completing it
            requestRewardedAd();
    }

    public static void OnRewardedAdLoaded()
    {
        m_adLoaded = true;
        CustomDebug.Log("Ad Loaded successfully");
    }

    public static void OnRewardedAdStarted()
    {

    }

    public static void processRewardedAdRewarded(string type, double amount)
    {
        CustomDebug.Log("Process rewarded ad rewarded");
        m_processReward = true;
        m_rewardString = type;
        m_rewardValue = amount;
    }

    public static void OnRewardedAdRewarded(string type, double amount)
    {
        CustomDebug.Log("Reward : type :" + type + "    amount : " + amount);
        switch (type)
        {
            case "soul":
                GameStats.SOULS += (int)amount;
                break;
            // Temp
            case "coins":
                GameStats.SOULS += 1;
                break;
        }

        processRequestCallback(type, amount);
    }

    public static void OnRewardedAdFailedToLoad()
    {
        requestRewardedAd();
    }

    public static void requestInterstitialAd()
    {
        m_googleAdsInterface.loadInterstitialAd();
    }

    public static void showInterstitialAd()
    {
        m_googleAdsInterface.showInterstitialAd();
    }

    public static void onInterstitialLoadFailed()
    {
        m_googleAdsInterface.loadInterstitialAd();
    }

    public static void onInterstitialClosed()
    {
        m_googleAdsInterface.loadInterstitialAd();
        m_interstitialAdCounter = 0;
    }
}
