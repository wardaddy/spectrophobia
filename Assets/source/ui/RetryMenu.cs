﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RetryMenu : MonoBehaviour {

    public Text m_retryText;
    public GameObject m_soulIcon;

    // Use this for initialization
    void OnEnable() {

        UserTracker.Instance.sendEvent("Popup", new Dictionary<string, object> {
                {"Action", "Retry Popup" },
                { "Souls", GameStats.SOULS},
                {"Score", GameManager.getInstance().getGlobalScore()},
                { "Highscore", GameStats.HIGHSCORE}
                // TODO: Number of retries in one go
            });
        UserTracker.Instance.LogEvent("Popup", "Retry Popup", "Souls : " + GameStats.SOULS + " Score : " + GameManager.getInstance().getGlobalScore() + " Highscore : " + GameStats.HIGHSCORE, 0);

        if (GameStats.SOULS >= PlayerDefs.CONST_RETRY_GAME_PRICE)
        {
            m_retryText.text = TextDefs.getText(TextDefs.TextId.TEXT_CONTINUE) + PlayerDefs.CONST_RETRY_GAME_PRICE;
            m_soulIcon.SetActive(true);
        }
        else
        {
            m_retryText.text = TextDefs.getText(TextDefs.TextId.TEXT_WATCH_AD);
            m_soulIcon.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update() {

    }

    public void OnClickYes()
    {
        if (TutorialManager.getInstance().isTutorialActive())
            return;

#if UNITY_ANDROID
        if (GameStats.SOULS >= PlayerDefs.CONST_RETRY_GAME_PRICE)
        {
            GameManager.getInstance().continueGame();
            UserTracker.Instance.sendEvent("Button", new Dictionary<string, object> {
                { "Action", "Yes Retry Popup"},
                { "Rewarded Video",false},
                { "Souls", GameStats.SOULS}
            });
            UserTracker.Instance.LogEvent("Button", "Yes Retry Popup", "Rewarded Video : " + false + " Souls : " + GameStats.SOULS, 0);
        }
        else
        {
            if (AdsManager.showRewardedAd(AdsManager.AD_REQUEST_ID.AD_REQUEST_CONTINUE_GAME) == false)
            {
                GameObject state = StateManager.getInstance().getPopupObject(StateManager.PopupType.POPUP_NO_AD_AVAILABLE);
                state.GetComponent<OKPopup>().setup(TextDefs.getText(TextDefs.TextId.TEXT_NO_ADS));
                StateManager.getInstance().pushPopup(StateManager.PopupType.POPUP_NO_AD_AVAILABLE);

                UserTracker.Instance.sendEvent("Error", new Dictionary<string, object> {
                    { "Action", "No Ads Available Retry Popup"},
                    { "State", StateManager.getInstance().getCurrentState()}
                });

                UserTracker.Instance.LogEvent("Error", "Rewarded Popup", "No Ads Available", 0);
            }
        }
#else
        if (GameStats.SOULS > PlayerDefs.CONST_RETRY_GAME_PRICE)
        {
            GameManager.getInstance().continueGame();
        }
        else
        {
            GameObject state = StateManager.getInstance().getPopupObject(StateManager.PopupType.POPUP_NOT_ENOUGH_SOUL);
            state.GetComponent<OKPopup>().setup("Not enough souls in inventory.");
            StateManager.getInstance().pushPopup(StateManager.PopupType.POPUP_NOT_ENOUGH_SOUL);
        }
#endif
    }

    public void onClickNo()
    {
        if (TutorialManager.getInstance().isTutorialActive())
            return;

        if (GameStats.IAP_COUNTER == 0 && AdsManager.m_interstitialAdCounter >= AdsManager.INTERSTIIAL_SHOW_AD_COUNTER)
        {
            AdsManager.showInterstitialAd();
        }
        StateManager.getInstance().pushState(StateManager.MenuState.STATE_RESULT);
        UserTracker.Instance.LogScreen("Retry Menu");
        UserTracker.Instance.sendEvent("Button", new Dictionary<string, object> {
                { "Action","No Retry Popup"},
                { "Score", GameManager.getInstance().getGlobalScore()},
                { "HighScore", GameStats.HIGHSCORE}
                // Totoal number of played
            });
        UserTracker.Instance.LogEvent("Button", "No Retry Popup", "Score : " + GameManager.getInstance().getGlobalScore() + "   HighScore : " + GameStats.HIGHSCORE, 0);
    }
}
