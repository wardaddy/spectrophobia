﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour {

    public Toggle m_autoAimToggle;
    public Toggle m_SFXToggle;
    public Toggle m_MusicToggle;

    // Use this for initialization
    void Start () {
		
	}

    private void OnEnable()
    {
        UserTracker.Instance.LogScreen("Settings Menu");
        UserTracker.Instance.sendEvent("Enter State", new Dictionary<string, object> {
                { "Action", "Settings"},
                { "AutoAim", GameSettings.AUTOAIM},
                { "SFX", GameSettings.SFX_ON },
                { "Music", GameSettings.MUSIC_ON}
            });
        UserTracker.Instance.LogEvent("Enter State", "Settings", "Auto Aim : " + GameSettings.AUTOAIM + "   SFX : " + GameSettings.SFX_ON + "   Music : " + GameSettings.MUSIC_ON, 0);
        m_autoAimToggle.isOn = GameSettings.AUTOAIM;
        m_SFXToggle.isOn = GameSettings.SFX_ON;
        m_MusicToggle.isOn = GameSettings.MUSIC_ON;

        if (TutorialManager.getInstance().canShow(TutorialManager.TutorialId.TUTORIAL_AUTO_AIM))
        {
            TutorialManager.getInstance().startTutorial(TutorialManager.TutorialId.TUTORIAL_AUTO_AIM, m_autoAimToggle.GetComponent<RectTransform>(), GetComponent<Canvas>());
            UserTracker.Instance.sendEvent("Tutorial", new Dictionary<string, object> {
                { "Action", "Auto Aim"},
                { "Auto Aim", GameSettings.AUTOAIM}
            });
            UserTracker.Instance.LogTutorialEvent(TutorialManager.TutorialId.TUTORIAL_AUTO_AIM);
        }

    }

    // Update is called once per frame
    void Update () {
        // back key
#if UNITY_ANDROID
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            StateManager.getInstance().pushState(StateManager.MenuState.STATE_MAIN);
        }
#endif
    }

    public void onClickAutoAim()
    {
        if (TutorialManager.getInstance().isTutorialActive())
            return;

        CustomDebug.Log("Enable Auto Aim : " + m_autoAimToggle.isOn);
        GameSettings.AUTOAIM = m_autoAimToggle.isOn;
        GameSettings.saveData();

        UserTracker.Instance.sendEvent("Button", new Dictionary<string, object> {
                { "Action", "AutoAim Settings"},
                { "Auto Aim", GameSettings.AUTOAIM}
            });
        UserTracker.Instance.LogEvent("Button", "Settings", "Auto Aim", GameSettings.AUTOAIM ? 1 : 0);
    }

    public void onClickSFX()
    {
        if (TutorialManager.getInstance().isTutorialActive())
            return;
        CustomDebug.Log("Enable Sound : " + m_SFXToggle.isOn);
        GameSettings.SFX_ON = m_SFXToggle.isOn;
        if (GameSettings.SFX_ON == false)
        {
            AudioManager.getInstance().stopSFX();
        }
        else
        {
            // play button click sound
        }
        GameSettings.saveData();

        UserTracker.Instance.sendEvent("Button", new Dictionary<string, object> {
                { "Action", "SFX Settings"},
                { "Auto Aim", GameSettings.SFX_ON}
            });
        UserTracker.Instance.LogEvent("Button", "Settings", "Auto Aim", GameSettings.AUTOAIM ? 1 : 0);
    }

    public void onClickMusic()
    {
        if (TutorialManager.getInstance().isTutorialActive())
            return;
        CustomDebug.Log("Enable Sound : " + m_MusicToggle.isOn);
        GameSettings.MUSIC_ON = m_MusicToggle.isOn;
        if (GameSettings.MUSIC_ON == false)
        {
            AudioManager.getInstance().stopAllMusic();
        }
        else
        {
            // play button click sound
            AudioManager.getInstance().playMusic(AudioManager.MUSIC.MUSIC_BG_MAIN_MENU);
        }
        GameSettings.saveData();
        UserTracker.Instance.sendEvent("Button", new Dictionary<string, object> {
                { "Action", "Music Settings"},
                { "Music", GameSettings.MUSIC_ON}
            });
        UserTracker.Instance.LogEvent("Button", "Settings", "Music", GameSettings.MUSIC_ON ? 1 : 0);
    }

    public void onClickBackButton()
    {
        if (TutorialManager.getInstance().isTutorialActive())
            return;
        StateManager.getInstance().pushState(StateManager.MenuState.STATE_MAIN);
    }
}
