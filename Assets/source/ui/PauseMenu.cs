﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour {

    public Toggle m_SFXToggle;
    public Toggle m_MusicToggle;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        // back key
#if UNITY_ANDROID
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            OnClickResume();
        }
#endif
    }

    public void OnEnable()
    {
        UserTracker.Instance.sendEvent("Enter State", new Dictionary<string, object> {
                { "Action", "Pause Menu"},
                { "SFX", GameSettings.SFX_ON},
                {"Music", GameSettings.MUSIC_ON}
            });
        UserTracker.Instance.LogScreen("Pause Menu");

        UserTracker.Instance.LogEvent("Enter State", "Pause Menu", "SFX : " + GameSettings.SFX_ON + "  Music : " + GameSettings.MUSIC_ON, 0);

        GameManager.getInstance().pauseGame(true);
        m_SFXToggle.isOn = GameSettings.SFX_ON;
        m_MusicToggle.isOn = GameSettings.MUSIC_ON;
    }

    public void OnClickResume()
    {
        if (TutorialManager.getInstance().isTutorialActive())
            return;
        GameManager.getInstance().pauseGame(false);
        StateManager.getInstance().pushState(StateManager.MenuState.STATE_HUD);
        GameManager.getInstance().enableRightJoystick(GameSettings.AUTOAIM == false);

        UserTracker.Instance.sendEvent("Button", new Dictionary<string, object> {
                {"Action", "Resumed"},
                { "Health", GameManager.getInstance().m_player.m_health}
            });

        UserTracker.Instance.LogEvent("Button", "Resumed", "Health : "+ GameManager.getInstance().m_player.m_health, 0);
    }

    public void OnClickExit()
    {
        if (TutorialManager.getInstance().isTutorialActive())
            return;

        UserTracker.Instance.sendEvent("Button", new Dictionary<string, object> {
                { "Action", "Exit Pause Menu" },
                { "Health", GameManager.getInstance().m_player.m_health},
                { "score", GameManager.getInstance().getGlobalScore()}
            // TODO:Number of time restarts
            });

        UserTracker.Instance.LogEvent("Button", "Exit Pause Menu", "Health : " + GameManager.getInstance().m_player.m_health + "    Score : " + GameManager.getInstance().getGlobalScore(), 0);
        GameManager.getInstance().exitGame();
        DerivedNetworManager.getInstance().stopHost();
       
    }

    public void onClickSFX()
    {
        if (TutorialManager.getInstance().isTutorialActive())
            return;
        CustomDebug.Log("Enable Sound : " + m_SFXToggle.isOn);
        GameSettings.SFX_ON = m_SFXToggle.isOn;
        if (GameSettings.SFX_ON == false)
        {
            AudioManager.getInstance().stopSFX();
        }
        GameSettings.saveData();

        UserTracker.Instance.sendEvent("Button", new Dictionary<string, object> {
                { "Action", "SFX Pause Menu" },
                { "SFX", GameSettings.SFX_ON}
            });
        UserTracker.Instance.LogEvent("Button", "SFX Pause Menu", "SFX :" + GameSettings.SFX_ON, 0);
    }

    public void onClickMusic()
    {
        if (TutorialManager.getInstance().isTutorialActive())
            return;
        CustomDebug.Log("Enable Sound : " + m_MusicToggle.isOn);
        GameSettings.MUSIC_ON = m_MusicToggle.isOn;
        if (GameSettings.MUSIC_ON == false)
        {
            AudioManager.getInstance().stopAllMusic();
        }
        else
        {
            AudioManager.getInstance().playMusic(AudioManager.MUSIC.MUSIC_BG_MAIN_MENU);
        }
        GameSettings.saveData();
        UserTracker.Instance.sendEvent("Button", new Dictionary<string, object> {
                { "Action", "Music Pause Menu"},
                { "Music", GameSettings.MUSIC_ON}
            });
        UserTracker.Instance.LogEvent("Button", "Music Pause Menu", "Music : " + GameSettings.MUSIC_ON, 0);
    }
}
