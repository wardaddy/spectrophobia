﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopMenu : MonoBehaviour {

	public List<StoreItem> m_storeItems;
	public GameObject m_errorText;
	// Use this for initialization
	void Start () {
		
	}

	void OnEnable() {
        UserTracker.Instance.LogScreen("Shop Menu");
        if (StoreManager.Instance.IsInitialized())
		{
			for (int i = 0; i < m_storeItems.Count; i++)
			{
				m_storeItems[i].gameObject.SetActive(true);
				StoreDataManager.StoreData data = StoreDataManager.Instance.m_productData[i];
				if(StoreManager.Instance.isAvailableForPurchase(data.m_productId))
					m_storeItems[i].setup(data);
			}
			m_errorText.SetActive(false);
		}
		else
		{
			StoreManager.Instance.InitializePurchasing();
			foreach (StoreItem item in m_storeItems)
			{
				item.gameObject.SetActive(false);
			}
			m_errorText.SetActive(true);
		}
	}

	// Update is called once per frame
	void Update () {
		
	}

	public void onClickedBackButton()
	{
		StateManager.getInstance().pushState(StateManager.MenuState.STATE_MAIN);
	}
}
