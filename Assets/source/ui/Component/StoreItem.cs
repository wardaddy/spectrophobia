﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoreItem : MonoBehaviour {

	private StoreDataManager.StoreData m_storeData;
	// Use this for initialization
	void Start () {
		
	}

	public void setup(StoreDataManager.StoreData storeData)
	{
		m_storeData = storeData;
		transform.Find("title").GetComponent<Text>().text = storeData.m_title;
		transform.Find("description").GetComponent<Text>().text = storeData.m_description;
		transform.Find("buy").Find("text").GetComponent<Text>().text = storeData.m_price;
	}	

	// Update is called once per frame
	void Update () {
		
	}

	public void onClickedBuy()
	{
        if (StoreManager.Instance.isAvailableForPurchase(m_storeData.m_productId))
        {
            StoreManager.Instance.BuyConsumable(m_storeData.m_productId);
        }
        else
        {
            UserTracker.Instance.LogException("Store not Available", true);
        }
	}
}
