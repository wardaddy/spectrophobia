﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConfirmationPopup : MonoBehaviour {

    public Text m_popupText;
    private StateManager.MenuState m_parentState = StateManager.MenuState.STATE_NONE;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnEnable()
    {
        UserTracker.Instance.LogScreen("Confirmation Popup");
    }

    public void setup(string text, StateManager.MenuState parentState = StateManager.MenuState.STATE_NONE)
    {
        UserTracker.Instance.sendEvent("Popup", new Dictionary<string, object> {
                { "Action","Confirmation"},
                { "Parent_State", StateManager.getInstance().getCurrentState()}
            });
        UserTracker.Instance.LogEvent("Popup", "Confirmation", "Parent_State : " + StateManager.getInstance().getCurrentState(), 0);
        m_popupText.text = text;
        m_parentState = parentState;
    }

    public void onCancel()
    {
        if (TutorialManager.getInstance().isTutorialActive())
            return;
        StateManager.getInstance().popPopup();

        UserTracker.Instance.sendEvent("Button", new Dictionary<string, object> {
                {"Action", "Cancel Confirmation"},
                { "Text", m_popupText.text}
            });
        UserTracker.Instance.LogEvent("Button", "Cancel Confirmation", "Popup : " + StateManager.getInstance().getCurrentPopup(), 0);
    }

    public void onOK()
    {
        if (TutorialManager.getInstance().isTutorialActive())
            return;

        UserTracker.Instance.sendEvent("Button", new Dictionary<string, object> {
                {"Action", "Yes Confirmation" },
                { "Text", m_popupText.text}
            });
        UserTracker.Instance.LogEvent("Button", "Yes Confiramtion", "Popup : " + StateManager.getInstance().getCurrentPopup(), 0);

        switch (StateManager.getInstance().getCurrentPopup())
        {
            case StateManager.PopupType.POPUP_CONFIRMATION_EXIT:
                // exit game
                UserTracker.Instance.sendEvent("Button", new Dictionary<string, object> {
                    { "Action","Exit Game"},
                    { "Souls", GameStats.SOULS}
                });
                UserTracker.Instance.LogEvent("Button", "Exit Game", "Souls", GameStats.SOULS);
                Application.Quit();
                return;
        }

        StateManager.getInstance().popPopup();
    }
}
