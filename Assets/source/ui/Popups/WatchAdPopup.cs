﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WatchAdPopup : MonoBehaviour {
    
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnEnable()
    {
        UserTracker.Instance.LogScreen("Watch Ad Menu");
    }

    public void onClickPlay()
    {
        if (TutorialManager.getInstance().isTutorialActive())
            return;

        if (AdsManager.showRewardedAd(AdsManager.AD_REQUEST_ID.AD_REQUEST_MAINMENU) == false)
        {
            GameObject state = StateManager.getInstance().getPopupObject(StateManager.PopupType.POPUP_NO_AD_AVAILABLE);
            state.GetComponent<OKPopup>().setup(TextDefs.getText(TextDefs.TextId.TEXT_NO_ADS));
            StateManager.getInstance().pushPopup(StateManager.PopupType.POPUP_NO_AD_AVAILABLE);
            UserTracker.Instance.sendEvent("Error", new Dictionary<string, object> {
                { "Action","Rewarded Ads"},
            });
            UserTracker.Instance.LogEvent("Error", "Rewarded Ads", "Not Loaded", 0);
        }
    }

    public void onClickClose()
    {
        if (TutorialManager.getInstance().isTutorialActive())
            return;

        UserTracker.Instance.LogEvent("Button", "Close", "Popup : " + StateManager.getInstance().getCurrentPopup(), 0);
        StateManager.getInstance().popPopup();
    }
}
