﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingPopup : MonoBehaviour {
	public Text m_text;
    public Image m_loadingImage;
    private float m_startTime;
    public float m_animSpeed = 0.1f;
    public float m_fillAmount = 0.1f;
	// Use this for initialization
	void Start () {
		
	}

    void OnEnable()
    {
        m_startTime = Time.time;
        UserTracker.Instance.LogScreen("Loading Menu");
    }

	public void setup(string text)
	{
		m_text.text = text;
	}

	// Update is called once per frame
	void Update () {

        if (Time.time - m_startTime > m_animSpeed)
        {
            m_startTime = Time.time;
            m_loadingImage.fillAmount += m_loadingImage.fillClockwise ? -m_fillAmount : m_fillAmount;
            if (m_loadingImage.fillAmount >= 1.0f || m_loadingImage.fillAmount <= 0.0f)
            {
                m_loadingImage.fillClockwise = !m_loadingImage.fillClockwise;
            }
        }
    }
}
