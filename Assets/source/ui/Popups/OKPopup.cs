﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OKPopup : MonoBehaviour {

    public Text m_popupText;
    private StateManager.MenuState m_parentState = StateManager.MenuState.STATE_NONE;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnEnable()
    {
        UserTracker.Instance.LogScreen("OK Menu");
    }

    public void setup(string text, StateManager.MenuState parentState = StateManager.MenuState.STATE_NONE)
    {
        m_popupText.text = text;
        m_parentState = parentState;
    }

    public void onOkClick()
    {
        if (TutorialManager.getInstance().isTutorialActive())
            return;

        switch (StateManager.getInstance().getCurrentPopup())
        {
            case StateManager.PopupType.POPUP_NO_AD_AVAILABLE:
			case StateManager.PopupType.POPUP_PURCHASE_SUCCESSFUL:
			case StateManager.PopupType.POPUP_PURCHASE_FAILED:
                    StateManager.getInstance().popPopup();
                break;

            case StateManager.PopupType.POPUP_RECEIVED_REWARD:
                {
                    StateManager.getInstance().popPopup();
                    StateManager.MenuState globalState = StateManager.getInstance().getCurrentState();
                    if (globalState == StateManager.MenuState.STATE_RETRY)
                    {
                        if (GameStats.SOULS >= PlayerDefs.CONST_RETRY_GAME_PRICE)
                        {
                            GameManager.getInstance().continueGame();
                            UserTracker.Instance.sendEvent("Button", new Dictionary<string, object> {
                                { "Action","OK Popup"},
                                { "Rewarded Video",true},
                                {"Souls", GameStats.SOULS}
                            });
                            UserTracker.Instance.LogEvent("Button", "OK Popup", "Rewarded Video", 0);
                        }
                        else
                        {
                            GameObject state = StateManager.getInstance().getPopupObject(StateManager.PopupType.POPUP_NOT_ENOUGH_SOUL);
                            state.GetComponent<OKPopup>().setup(TextDefs.getText(TextDefs.TextId.TEXT_NOT_ENOUGH_SOUL));
                            StateManager.getInstance().pushPopup(StateManager.PopupType.POPUP_NOT_ENOUGH_SOUL);
                        }
                    }
                    else if (globalState == StateManager.MenuState.STATE_MAIN)
                    {
                        if (GameStats.SOULS >= PlayerDefs.CONST_START_GAME_PRICE)
                        {
                            GameManager.getInstance().startSinglePlayerGame();
                        }
                        else
                        {
                            GameObject state = StateManager.getInstance().getPopupObject(StateManager.PopupType.POPUP_NOT_ENOUGH_SOUL);
                            state.GetComponent<OKPopup>().setup(TextDefs.getText(TextDefs.TextId.TEXT_NOT_ENOUGH_SOUL));
                            StateManager.getInstance().pushPopup(StateManager.PopupType.POPUP_NOT_ENOUGH_SOUL);
                        }
                    }
                }
                break;
            case StateManager.PopupType.POPUP_NOT_ENOUGH_SOUL:
                {
                    StateManager.getInstance().popPopup();
                    if(StateManager.getInstance().getCurrentState() == StateManager.MenuState.STATE_RETRY)
                       StateManager.getInstance().pushState(StateManager.MenuState.STATE_RESULT);
                }
                break;
        }
    }
}
