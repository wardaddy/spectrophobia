﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MainMenu : MonoBehaviour {

    private bool toggleAutoAim = true;
    public GameObject m_singlePlayerBtn;
    public GameObject m_multiplayerBtn;
    public GameObject m_setttingBtn;
    public GameObject m_lifeBtn;
    public GameObject m_GPGSBtn;
    public GameObject m_lbBtn;
    public Text m_soulCount;
    public Text m_soulTimer;
    
	// Use this for initialization
	void Start () {
        
	}

    private void OnEnable()
    {
        UserTracker.Instance.sendEvent("Enter State", new Dictionary<string, object> {
                { "Action","MainMenu"},
                { "Souls", GameStats.SOULS}
            });

        UserTracker.Instance.LogMainMenuEvent();
        UserTracker.Instance.LogScreen("Main Menu");

#if !ENABLE_MULTIPLAYER
        m_multiplayerBtn.SetActive(false);
#endif

#if !UNITY_ANDROID
        m_GPGSBtn.SetActive(false);
        m_lbBtn.SetActive(false);
#endif
        AudioManager.getInstance().playMusic(AudioManager.MUSIC.MUSIC_BG_MAIN_MENU);
    }
    // Update is called once per frame
    void Update () {
        if (TutorialManager.getInstance().canShow(TutorialManager.TutorialId.TUTORIAL_PLAY))
        {
            TutorialManager.getInstance().startTutorial(TutorialManager.TutorialId.TUTORIAL_PLAY, m_singlePlayerBtn.GetComponent<RectTransform>(), GetComponent<Canvas>());
            UserTracker.Instance.sendEvent("Tutorial", new Dictionary<string, object> {
                { "Action"," Start Game"},
                { "Souls", GameStats.SOULS}
            });

            UserTracker.Instance.LogTutorialEvent(TutorialManager.TutorialId.TUTORIAL_PLAY);
        }

        if (TutorialManager.getInstance().canShow(TutorialManager.TutorialId.TUTORIAL_SETTINGS))
        {
            TutorialManager.getInstance().startTutorial(TutorialManager.TutorialId.TUTORIAL_SETTINGS, m_setttingBtn.GetComponent<RectTransform>(), GetComponent<Canvas>());
            UserTracker.Instance.sendEvent("Tutorial", new Dictionary<string, object> {
                { "Action", "Settings"},
                { "Souls", GameStats.SOULS}
            });
            UserTracker.Instance.LogTutorialEvent(TutorialManager.TutorialId.TUTORIAL_SETTINGS);
        }

        if (TutorialManager.getInstance().canShow(TutorialManager.TutorialId.TUTORIAL_LIFE_REGENERATION) && GameStats.SOULS < PlayerDefs.CONST_MAX_SOUL)
        {
            TutorialManager.getInstance().startTutorial(TutorialManager.TutorialId.TUTORIAL_LIFE_REGENERATION, m_lifeBtn.GetComponent<RectTransform>(), GetComponent<Canvas>());
            UserTracker.Instance.sendEvent("Tutorial", new Dictionary<string, object> {
                { "Action", "Life Regenration"},
                { "Souls", GameStats.SOULS}
            });
            UserTracker.Instance.LogTutorialEvent(TutorialManager.TutorialId.TUTORIAL_LIFE_REGENERATION);
        }

        if (GameStats.SOULS < PlayerDefs.CONST_MAX_SOUL)
        {
            m_soulTimer.transform.parent.gameObject.SetActive(true);
            float remainingTime = Timer.getInstance().getRemainingTime(Timer.TIMER_ID.TIMER_SOUL_REGENERATION);
            int min = (int)(remainingTime / 60.0f);
            int sec = (int)(remainingTime % 60.0f);
            m_soulTimer.text = "" + min + " : " + sec;
        }
        else
        {
            m_soulTimer.transform.parent.gameObject.SetActive(false);
        }

        m_soulCount.text = "" + GameStats.SOULS;

        // Exit Confirmation
#if UNITY_ANDROID
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            GameObject state = StateManager.getInstance().getPopupObject(StateManager.PopupType.POPUP_CONFIRMATION_EXIT);
            state.GetComponent<ConfirmationPopup>().setup(TextDefs.getText(TextDefs.TextId.TEXT_EXIT));
            StateManager.getInstance().pushPopup(StateManager.PopupType.POPUP_CONFIRMATION_EXIT);
        }
#endif
    }

    public void OnGUI()
    {
        if (CustomDebug.isDebugBuild())
        {
            if (GUI.Button(new Rect(10, 110, 100, 100), "add soul"))
            {
                GameStats.SOULS += 1;
                CustomDebug.Log("Gamestats Soul: " + GameStats.SOULS);
            }

#if UNITY_ANDROID
            if (GUI.Button(new Rect(10, 210, 100, 100), "Upload Progress"))
            {
#if UNITY_ANDROID
                if (GPGSInterface.Instance.isAuthenticated())
#endif
                    GPGSInterface.Instance.uploadProgress();
            }

            if (GUI.Button(new Rect(10, 310, 100, 100), "Load Progress"))
            {
#if UNITY_ANDROID
                if (GPGSInterface.Instance.isAuthenticated())
#endif
                    GPGSInterface.Instance.loadProgress();
            }
#endif
            if (GUI.Button(new Rect(new Vector2(Screen.width - 100, Screen.height - 100), new Vector2(100, 50)), "Reset Tutorial"))
            {
                TutorialManager.getInstance().resetTutorial();
            }

			if (GUI.Button(new Rect(new Vector2(10, Screen.height - 100), new Vector2(100, 50)), "Reset Data"))
			{
				GameStats.SOULS = 0;
                GameStats.IAP_COUNTER = 0;
			#if UNITY_ANDROID
				if (GPGSInterface.Instance.isAuthenticated())	
					GPGSInterface.Instance.uploadProgress();
			#endif
			}

		}
	}

    public void onClickSinglePlayer()
    {
#if UNITY_ANDROID
        if (GameStats.SOULS < PlayerDefs.CONST_START_GAME_PRICE)
            StateManager.getInstance().pushPopup(StateManager.PopupType.POPUP_WATCH_AD);
        else
#endif
        {
            GameManager.getInstance().startSinglePlayerGame();
        }
        UserTracker.Instance.sendEvent("Button", new Dictionary<string, object> {
                {"Action", "Start Game" },
                { "Souls", GameStats.SOULS},
                { "Auto Aim", GameSettings.AUTOAIM }
            });
        UserTracker.Instance.LogEvent("Button", "Start Game", "Souls", GameStats.SOULS);
        UserTracker.Instance.LogEvent("Button", "Start Game", "Auto Aim", GameSettings.AUTOAIM ? 1 : 0);
    }

    public void onClickMultiplayer()
    {
        if (TutorialManager.getInstance().isTutorialActive())
            return;
        GameManager.getInstance().setGameplayMode(GameManager.GameplayMode.MULTIPLAYER);

        StateManager.getInstance().pushState(StateManager.MenuState.STATE_MULTIPLAYER);
    }

    public void onClickSettings()
    {
        if (TutorialManager.getInstance().isTutorialActive())
            return;
        StateManager.getInstance().pushState(StateManager.MenuState.STATE_SETTINGS);
    }

    public void onClickGoogleLogin()
    {
        if (TutorialManager.getInstance().isTutorialActive())
            return;
#if UNITY_ANDROID
        GPGSInterface.Instance.loginGPGS();
        //GooglePlayGamesManager.Instance.Login();
        UserTracker.Instance.sendEvent("Button", new Dictionary<string, object> {
                {"Action", "G+" },
                { "Authenticated", GPGSInterface.Instance.isAuthenticated()}
            });
        UserTracker.Instance.LogSocial("Google", "Sign In", "Authenticated : " + GPGSInterface.Instance.isAuthenticated());
#endif
        
    }

    public void onClickGlobalLB()
    {
        if (TutorialManager.getInstance().isTutorialActive())
            return;
#if UNITY_ANDROID
        GPGSInterface.Instance.showGlobalLeaderboard();
        UserTracker.Instance.sendEvent("Button", new Dictionary<string, object> {
                { "Action","Leaderboard MainMenu" },
                { "Authenticated", GPGSInterface.Instance.isAuthenticated()}
            });

        UserTracker.Instance.LogSocial("Google", "Leaderboard", "Authenticated : " + GPGSInterface.Instance.isAuthenticated());
#endif
    }

	public void onClickShop()
	{
		if (TutorialManager.getInstance().isTutorialActive())
			return;
#if UNITY_ANDROID
        if (GPGSInterface.Instance.isAuthenticated() == false)
            GPGSInterface.Instance.loginGPGS();
        else
#endif
            StateManager.getInstance().pushState(StateManager.MenuState.STATE_SHOP);
    }
}
