﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultMenu : MonoBehaviour {

    public Text m_playerScore;
    public Text m_highScore;
    public GameObject m_rewardText;
    public GameObject m_lbBtn;
	// Use this for initialization
	void Start () {

	}

    private void OnEnable()
    {
        UserTracker.Instance.LogScreen("Result Menu");
        UserTracker.Instance.sendEvent("Enter State", new Dictionary<string, object> {
                { "Action", "Result"},
                { "Score", GameManager.getInstance().getGlobalScore()},
                {"Highscore", GameStats.HIGHSCORE },
                { "Auto Aim", GameSettings.AUTOAIM}
            });
        UserTracker.Instance.LogEvent("Enter State", "Result", "Score : " + GameManager.getInstance().getGlobalScore() + " Highscore : " + GameStats.HIGHSCORE + " AutoAim : " + GameSettings.AUTOAIM, 0);

        if (GameManager.getInstance().getGlobalScore() > GameStats.HIGHSCORE)
        {
            GameStats.HIGHSCORE = GameManager.getInstance().getGlobalScore();
            m_rewardText.SetActive(true);
        }
        else
        {
            m_rewardText.SetActive(false);
        }

        m_playerScore.text = "Your Score : " + GameManager.getInstance().getGlobalScore();
        m_highScore.text = "HighScore : " + GameStats.HIGHSCORE;

		#if UNITY_ANDROID
		if (CustomDebug.isDebugBuild() == false)
		{
            if (GameSettings.AUTOAIM)
            {
                GPGSInterface.Instance.postAutoAimScore(GameManager.getInstance().getGlobalScore());
                UserTracker.Instance.sendEvent("Social", new Dictionary<string, object> {
                    { "Action", "Post Score"},
                    { "Leaderboard", "Auto Aim"},
                    { "Score", GameManager.getInstance().getGlobalScore()}
                });

                UserTracker.Instance.LogEvent("Social", "Post Score", "Auto Aim Leaderboard", GameManager.getInstance().getGlobalScore());
            }
            else
            {
                GPGSInterface.Instance.postScore(GameManager.getInstance().getGlobalScore());
                UserTracker.Instance.sendEvent("Social", new Dictionary<string, object> {
                    { "Action", "Post Score"},
                    { "Leaderboard", "Normal"},
                    { "Score", GameManager.getInstance().getGlobalScore()}
                });
                UserTracker.Instance.LogEvent("Social", "Post Score", "Normal Leaderboard", GameManager.getInstance().getGlobalScore());
            }
		}
		#else
			m_lbBtn.SetActive(false);
		#endif
    }
    // Update is called once per frame
    void Update () {
		
	}

    public void onClickOK()
    {
        if (TutorialManager.getInstance().isTutorialActive())
            return;
        GameManager.getInstance().exitGame();
        DerivedNetworManager.getInstance().stopHost();

        UserTracker.Instance.sendEvent("Button", new Dictionary<string, object> {
                {"Action", "OK Result Popup" },
                { "Score", GameManager.getInstance().getGlobalScore()}
            });
        UserTracker.Instance.LogEvent("Button", "OK Result Popup", "Score", GameManager.getInstance().getGlobalScore());
    }

    public void onClickGlobalLB()
    {
        if (TutorialManager.getInstance().isTutorialActive())
            return;
        #if UNITY_ANDROID
        GPGSInterface.Instance.showGlobalLeaderboard();
        UserTracker.Instance.sendEvent("Button", new Dictionary<string, object> {
                {"Action", "LB Result Popup" },
                { "Authenticated", GPGSInterface.Instance.isAuthenticated()}
            });
        UserTracker.Instance.LogEvent("Button", "LB Result Popup", "Authenticated", GPGSInterface.Instance.isAuthenticated() ? 1 : 0);
#endif
    }
}
