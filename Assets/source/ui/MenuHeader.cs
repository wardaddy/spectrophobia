﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class MenuHeader : MonoBehaviour {

    public Text     m_candyCount;
    public Text     m_globalScore;

    public Text     m_xp;
    public GameObject m_joystick;
    public GameObject m_leftJoystick;
    public GameObject m_rightJoystick;

	// Use this for initialization
	void Start () {
		
	}

    private void OnEnable()
    {
        m_joystick.SetActive(true);
    }

    private void OnDisable()
    {
        m_joystick.SetActive(false);
    }

    // Update is called once per frame
    void Update () {

        // Tutorial Check
        if (TutorialManager.getInstance().canShow(TutorialManager.TutorialId.TUTORIAL_JOYSTICK_LEFT))
        {
            TutorialManager.getInstance().startTutorial(TutorialManager.TutorialId.TUTORIAL_JOYSTICK_LEFT, m_leftJoystick.GetComponent<RectTransform>(), GetComponent<Canvas>());
        }

        if (GameSettings.AUTOAIM == false)
        {
            if (TutorialManager.getInstance().canShow(TutorialManager.TutorialId.TUTORIAL_JOYSTICL_RIGHT))
            {
                TutorialManager.getInstance().startTutorial(TutorialManager.TutorialId.TUTORIAL_JOYSTICL_RIGHT, m_rightJoystick.GetComponent<RectTransform>(), GetComponent<Canvas>());
            }
        }
        
        m_candyCount.text = "" + GameStats.SOULS;
        m_globalScore.text = "" + GameManager.getInstance().getGlobalScore();
        m_xp.text = "" + GameManager.getInstance().getXp();

        // Back button
#if UNITY_ANDROID
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            StateManager.getInstance().pushState(StateManager.MenuState.STATE_PAUSE);
        }
#endif
    }

    public void onClickPause()
    {
        if (TutorialManager.getInstance().isTutorialActive())
            return;
        CustomDebug.Log("on Click Pause");
        StateManager.getInstance().pushState(StateManager.MenuState.STATE_PAUSE);
    }
}
