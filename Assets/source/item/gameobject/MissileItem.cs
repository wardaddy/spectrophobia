﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileItem : ItemBase {
    public int m_missileCount = 10;

    protected override void Start()
    {
        base.Start();
        m_type = ItemManager.ITEM_TYPE.ITEM_M;
    }

    protected override void Update()
    {
        base.Update();
    }

    protected override void OnTriggerEnter2D(Collider2D col)
    {
        base.OnTriggerEnter2D(col);
    }
}
