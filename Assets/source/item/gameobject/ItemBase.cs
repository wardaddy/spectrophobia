﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class ItemBase : MonoBehaviour {

	public ItemManager.ITEM_TYPE m_type;
	public float 	m_spawnDuration;
	protected float	m_startTime;
    protected bool m_isCollected = false;
    public float    m_activeDuration;

	// Use this for initialization
	protected virtual void Start () {
        m_startTime = Time.time;
        UserTracker.Instance.sendEvent("Item", new Dictionary<string, object> {
                { "Action", "Spawned"},
                { "Type", m_type}
            });
        UserTracker.Instance.LogEvent("Item", "Spawned Item", "Type", (int)m_type);
    }
	
	// Update is called once per frame
	protected virtual void Update () {
        if (GameManager.getInstance().isGamePaused())
            return;

        if (Time.time - m_startTime > m_spawnDuration)
        {
            AnimData animData = new AnimData();
            animData.m_duration = 0.3f;
            animData.m_position = transform.position;
            AnimationManager.getInstance().startAnim(AnimationManager.AnimType.ANIM_ITEM_DISAPPEAR, animData);

            UserTracker.Instance.sendEvent("Item", new Dictionary<string, object> {
                { "Action", "Disappeared"},
                { "Type", m_type}
            });
            UserTracker.Instance.LogEvent("Item", "Item Disappeared", "Type", (int)m_type);

            Destroy(gameObject);
        }
    }

    protected virtual void FixedUpdate()
    {
        if (GameManager.getInstance().isGamePaused())
            return;

        // move item downwards (as in under gravity)
        transform.position = Vector3.Lerp(transform.position, transform.position - Vector3.up, Time.deltaTime * 0.25f);
    }

    protected virtual void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.layer == LayerMask.NameToLayer("player"))
        {
            m_isCollected = true;
            ItemManager.getInstance().addItem(this);
            AudioManager.getInstance().playSFX(AudioManager.SFX.SFX_ITEM_COLLECTED);
            //Anim
            Vector3 targetScale = transform.localScale + new Vector3(4.0f, 4.0f, 0.0f);
            BlastAnimData data = new BlastAnimData(transform, targetScale, 0.5f);
            AnimationManager.getInstance().startAnim(AnimationManager.AnimType.ANIM_ITEM_COLLECT, data);

            UserTracker.Instance.sendEvent("Item", new Dictionary<string, object> {
                { "Action", "Collected"},
                { "Type", m_type}
            });
            UserTracker.Instance.LogEvent("Item", "Item Collected", "Type", (int)m_type);

            Destroy(gameObject);
        }
    }

    public ItemManager.ITEM_TYPE getItemType() {
		return m_type;
	}

}