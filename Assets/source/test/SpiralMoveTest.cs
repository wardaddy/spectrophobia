﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiralMoveTest : MonoBehaviour {
    public float CurveSpeed = 5;
    public float MoveSpeed = 1;
    float fTime = 0;
    public float m_amplitude = 1.0f;
    Vector3 vLastPos = Vector3.zero;

    public Transform m_startPos;
    public Transform m_endPos;

    // Use this for initialization
    void Start () {
        transform.position = m_startPos.position;
        vLastPos = transform.position;
    }

    // Update is called once per frame
    void Update () {
        vLastPos = transform.position;

        if (fTime > Mathf.PI * 2.0f)
            fTime = 0.0f;

        fTime += Time.deltaTime * CurveSpeed;

        Vector3 vSin = new Vector3(Mathf.Sin(fTime) * m_amplitude, -Mathf.Sin(fTime) * m_amplitude, 0);
        Vector3 vLin = new Vector3(MoveSpeed, MoveSpeed, 0);
        Vector3 dir = (m_endPos.position - transform.position).normalized * MoveSpeed;

        transform.position += ( dir + vSin) * Time.deltaTime;

        //Debug.DrawLine(vLastPos, transform.position, Color.green, 100);

    }

    public void OnGUI()
    {
        if(GUI.Button(new Rect(10, 10, 100, 100), "Reset"))
        {
            transform.position = m_startPos.position;
        }
    }
}
