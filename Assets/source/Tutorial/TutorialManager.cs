﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;

[System.Serializable]
public class TutorialDictionary : DictionaryTemplate<TutorialManager.TutorialId, GameObject> { }

public class TutorialManager : MonoBehaviour {

    //public Image BG;
    public enum TutorialId {
        TUTORIAL_NONE,
        TUTORIAL_PLAY,
        TUTORIAL_SETTINGS,
        TUTORIAL_LIFE_REGENERATION,
        TUTORIAL_AUTO_AIM,
        TUTORIAL_POWERUP,
        TUTORIAL_JOYSTICK_LEFT,
        TUTORIAL_JOYSTICL_RIGHT,
        TUTORIAL_HEALTH,
        TUTORIAL_POWER,
        TUTORIAL_COUNT,
    }

    private static TutorialManager m_instance;
    public static TutorialManager getInstance() { return m_instance; }

    private static string FILENAME = "/tutorial.dat";
   
    private static int[] m_tutorialStatus;
    private bool m_isTutorialActive = false;
    private TutorialId m_activeTutorialId = TutorialId.TUTORIAL_NONE;

    public List<TutorialDictionary> m_tutorialPrefabs;

    private void Awake()
    {
        m_instance = this;
        m_tutorialStatus = new int[(int)TutorialId.TUTORIAL_COUNT];
    }

    // Use this for initialization
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {

    }

    private void OnEnable()
    {

    }

    private GameObject getTutorialPrefab(TutorialId id)
    {
        var obj = m_tutorialPrefabs.Find(item => item._key == id);
        if (obj != null)
            return obj._value;
        return null;
    }

    public void startTutorial(TutorialId id, RectTransform rectTransform, Canvas canvas)
    {
        //BG.gameObject.SetActive(true);
        m_isTutorialActive = true;
        m_activeTutorialId = id;

        GameObject tutorialPrefab = getTutorialPrefab(id);
        tutorialPrefab.transform.Find("text").GetComponent<Text>().text = getTutText(id);
        tutorialPrefab.SetActive(true);
        GameManager.getInstance().pauseGame(true);
        Time.timeScale = 0.0f;
    }

    public void startTutorial(TutorialId id, Rect maskRect)
    {
        m_isTutorialActive = true;
        m_activeTutorialId = id;

        GameObject tutorialPrefab = getTutorialPrefab(id);
        tutorialPrefab.transform.Find("text").GetComponent<Text>().text = getTutText(id);
        tutorialPrefab.SetActive(true);
        GameManager.getInstance().pauseGame(true);
        Time.timeScale = 0.0f;
    }

    private void onOK()
    {
        m_isTutorialActive = false;
        m_tutorialStatus[(int)m_activeTutorialId] = 1;
        getTutorialPrefab(m_activeTutorialId).SetActive(false);
        m_activeTutorialId = TutorialId.TUTORIAL_NONE;
        GameManager.getInstance().pauseGame(false);

        Time.timeScale = 1.0f;
    }

    private void OnGUI()
    {
        if (m_activeTutorialId == TutorialId.TUTORIAL_NONE)
            return;

        Color color = GUI.backgroundColor;
        GUI.backgroundColor = Color.clear;

        if (GUI.Button(new Rect(new Vector2(0, 0), new Vector2(Screen.width, Screen.height)), ""))
        {
            onOK();
        }
        
        GUI.backgroundColor = color;
    }

    public void resetTutorial()
    {
        for (int i = 0; i < (int)TutorialId.TUTORIAL_COUNT; i++)
            m_tutorialStatus[i] = 0;
    }

    public static void saveData()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream f = File.Create(Application.persistentDataPath + FILENAME);
        bf.Serialize(f, m_tutorialStatus);
        f.Close();

    }

    public static void loadData()
    {
        if (File.Exists(Application.persistentDataPath + FILENAME))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream f = File.Open(Application.persistentDataPath + FILENAME, FileMode.Open);
            int[] status = (int[])bf.Deserialize(f);
            for (int i = 0; i < status.Length; i++)
            {
                m_tutorialStatus[i] = status[i];
            }
            f.Close();
        }
        else
        {
            for (int i = 0; i < (int)TutorialId.TUTORIAL_COUNT; i++)
            {
                m_tutorialStatus[i] = 0;
            }
            CustomDebug.Log("Game Tutorial File does not exist");
        }

    }

    public bool isTutorialActive()
    { return m_isTutorialActive; }

    public TutorialId getActiveTutorialId()
    { return m_activeTutorialId; }

    public bool canShow(TutorialId id)
    {
        return (m_tutorialStatus[(int)id] == 0) && (m_isTutorialActive == false);
    }

    private string getTutText(TutorialId id)
    {
        switch (id)
        {
            case TutorialId.TUTORIAL_AUTO_AIM:
                return TextDefs.getText(TextDefs.TextId.TEXT_TUTORIAL_AUTO_AIM);
            case TutorialId.TUTORIAL_HEALTH:
                return TextDefs.getText(TextDefs.TextId.TEXT_TUTORIAL_HEALTH);
            case TutorialId.TUTORIAL_JOYSTICK_LEFT:
                return TextDefs.getText(TextDefs.TextId.TEXT_TUTORIAL_JOYSTICK_LEFT);
            case TutorialId.TUTORIAL_JOYSTICL_RIGHT:
                return TextDefs.getText(TextDefs.TextId.TEXT_TUTORIAL_JOYSTICK_RIGHT);
            case TutorialId.TUTORIAL_LIFE_REGENERATION:
                return TextDefs.getText(TextDefs.TextId.TEXT_TUTORIAL_LIFE_REGENRATION);
            case TutorialId.TUTORIAL_PLAY:
                return TextDefs.getText(TextDefs.TextId.TEXT_TUTORIAL_PLAY);
            case TutorialId.TUTORIAL_POWER:
                return TextDefs.getText(TextDefs.TextId.TEXT_TUTORIAL_POWER);
            case TutorialId.TUTORIAL_POWERUP:
                return TextDefs.getText(TextDefs.TextId.TEXT_TUTORIAL_POWER_UP);
            case TutorialId.TUTORIAL_SETTINGS:
                return TextDefs.getText(TextDefs.TextId.TEXT_TUTORIAL_SETTINGS);
        }
        return "";
    }
}
