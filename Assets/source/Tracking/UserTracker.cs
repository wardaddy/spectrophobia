﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class UserTracker : Singleton<UserTracker> {

    public GoogleAnalyticsV4 m_googleAnalytics;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void init()
    {
        m_googleAnalytics.StartSession();
    }

    public void sendEvent(string eventName, Dictionary<string, object> parameters)
    {
        Analytics.CustomEvent(eventName, parameters);
    }

    public void LogScreen(string screenName)
    {
        m_googleAnalytics.LogScreen(screenName);
    }

    public void LogEvent(string eventCategory, string eventAction, string eventLabel, long value)
    {
        EventHitBuilder builder = new EventHitBuilder();
        builder.SetEventCategory(eventCategory);
        builder.SetEventAction(eventAction);
        builder.SetEventLabel(eventLabel);
        builder.SetEventValue(value);
        m_googleAnalytics.LogEvent(builder);
    }

    public void LogEvent(EventHitBuilder builder)
    {
        m_googleAnalytics.LogEvent(builder);
    }

    public void LogException(string exceptionDescription, bool isFatal)
    {
        m_googleAnalytics.LogException(exceptionDescription, isFatal);
    }

    public void LogSocial(string socialNetwork, string socialAction, string socialTarget = "")
    {
        m_googleAnalytics.LogSocial(socialNetwork, socialAction, socialTarget);
    }

    public void LogTransaction(string transId, string affiliation, double revenue, string currencyCode)
    {
        TransactionHitBuilder builder = new TransactionHitBuilder();
        builder.SetTransactionID(transId);
        builder.SetAffiliation(affiliation);
        builder.SetRevenue(revenue);
        builder.SetCurrencyCode(currencyCode);
        m_googleAnalytics.LogTransaction(builder);
    }

    public void LogItem(string transId, string name, string category, double price, long quantity)
    {
        ItemHitBuilder builder = new ItemHitBuilder();
        builder.SetTransactionID(transId);
        builder.SetName(name);
        builder.SetCategory(category);
        builder.SetPrice(price);
        builder.SetQuantity(quantity);

        m_googleAnalytics.LogItem(builder);
    }

    public void LogMainMenuEvent()
    {
        EventHitBuilder builder = new EventHitBuilder();
        builder.SetEventCategory("Menu");
        builder.SetEventAction("Entered Menu");
        builder.SetEventLabel("Main Menu");
        builder.SetCustomDimension(GameStats.SOULS, "Souls");
        LogEvent(builder);
    }

    public void LogTutorialEvent(TutorialManager.TutorialId id)
    {
        EventHitBuilder builder = new EventHitBuilder();
        builder.SetEventCategory("Tutorial");
        builder.SetEventAction("Tutorial triggered");

        switch (id)
        {
            case TutorialManager.TutorialId.TUTORIAL_PLAY:
                builder.SetEventLabel("Play");
                break;
            case TutorialManager.TutorialId.TUTORIAL_SETTINGS:
                builder.SetEventLabel("Settings");
                break;
            case TutorialManager.TutorialId.TUTORIAL_LIFE_REGENERATION:
                builder.SetEventLabel("Life Regeneration");
                break;
            case TutorialManager.TutorialId.TUTORIAL_JOYSTICK_LEFT:
                builder.SetEventLabel("Left Joystick");
                break;
            case TutorialManager.TutorialId.TUTORIAL_JOYSTICL_RIGHT:
                builder.SetEventLabel("Right Joystick");
                break;
            case TutorialManager.TutorialId.TUTORIAL_HEALTH:
                builder.SetEventLabel("Health");
                break;
            case TutorialManager.TutorialId.TUTORIAL_AUTO_AIM:
                builder.SetEventLabel("Auto Aim");
                break;
            case TutorialManager.TutorialId.TUTORIAL_POWER:
                builder.SetEventLabel("Power");
                break;
        }
        LogEvent(builder);
    }
}
