﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundAnimation : MonoBehaviour {
    public float m_speed = 0.1f;
    public float m_time = 2.0f;
    private float m_startTime;
    private Vector3 m_targetPos;
    // Use this for initialization
    void Start()
    {
        m_startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - m_startTime > m_time)
        {
            m_targetPos = new Vector3(Random.Range(-8.0f, 8.0f), Random.Range(-3.5f, 5.5f), transform.position.z);
            m_startTime = Time.time;
        }

        transform.position = Vector3.Lerp(transform.position, m_targetPos, Time.deltaTime * m_speed);
    }
}
