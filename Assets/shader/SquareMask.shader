﻿Shader "Custom/SquareMask"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_MaskRect("MaskRect", vector) = (0.0000, 0.0000, 0.0000, 0.0000)
		//_Color("Color",Color) = (1,1,1,1)
	}
	SubShader
	{
		Tags{ "RenderType" = "Opaque" }
		LOD 100

		
		
		pass
		{
			Blend SrcAlpha OneMinusSrcAlpha
				CGPROGRAM

#pragma vertex vert
#pragma fragment frag
#include "UnityCG.cginc"

				struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			vector _MaskRect;
			//Color _Color;

			v2f vert(appdata IN)
			{
				v2f o;
				float4 pos = UnityObjectToClipPos(IN.vertex);
				o.vertex = pos;
				o.uv = IN.uv;
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				// sample the texture
				//fixed4 color = tex2D(_MainTex, i.uv);
			float2 tc = i.uv;
			float x = -(_MaskRect.x + _MaskRect.z);
			float y = -(_MaskRect.y + _MaskRect.w);
			float2 p = float2(x,y) + 2.0 * tc;
			float len = length(p);
			float2 uv = tc + (p / len) * _Time.y * 0.1 /** cos(len * 12.0 - _Time.y * 10.0) * 0.03*/;
			fixed4 col = tex2D(_MainTex, uv);

			if (i.uv.x > _MaskRect.x && i.uv.x < _MaskRect.z && i.uv.y >_MaskRect.y && i.uv.y < _MaskRect.w)
				col.a = 0.0;

			return col;// *color;
			}
				ENDCG

		}

	}
}
